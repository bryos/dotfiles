(use-modules
 ((channel-b packages python-xyz) #:prefix br:)
 (gnu packages machine-learning)
 (gnu packages python)
 (gnu packages python-science)
 (gnu packages python-web)
 (gnu packages python-xyz)
 )

(packages->manifest
 (list
  ;; python-notebook                       ;provides jupyter-notebook
  ;; python-pygame ;box2d require newer 2.5 version
  ;; br:python-box2d
  ;; br:python-gymnasium
  ;; python-matplotlib
  ;; provides wrappers for python 3.x s.t. `python' invokes `python3'
  ;; and the same for `pip3'. Do *not* install the `python' package
  ;; when using `python-wrapper'.
  python-wrapper
  python-numpy
  python-requests
  ;; python-pandas
  ;; python-scikit-learn
  ;; python-scipy
  ;; python-seaborn
  ))
