(use-modules (gnu))
(use-package-modules
  image-viewers
  imagemagick
  linux
  pdf
  pulseaudio
  video
  )

(packages->manifest
 (list
  ;; images
  feh
  imagemagick
  ;; video
  mpv
  ffmpeg
  yt-dlp
  ;; pdf
  zathura
  zathura-pdf-poppler
  ;; sound
  alsa-utils
  pavucontrol
  ))
