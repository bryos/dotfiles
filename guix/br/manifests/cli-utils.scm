(use-modules (gnu))
(use-package-modules
  aidc
  base
  compression
  curl
  file
  freedesktop
  haskell-apps
  python-xyz
  rust-apps
  wget
  )

(packages->manifest
 (list
  ;; make
  ;; cmake

  file
  ripgrep

  shellcheck
  python-tldr

  udisks

  unzip
  zip

  ;; scrot
  zbar

  curl
  wget
  ))
