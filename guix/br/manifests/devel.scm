(use-modules (gnu))
(use-package-modules
  base
  commencement
  curl
  man
  version-control
  wget
 )

(packages->manifest
 (list
  gnu-make
  ;; cmake
  gcc-toolchain
  git

  man-db
  man-pages
  man-pages-posix

  curl
  wget
  ))
