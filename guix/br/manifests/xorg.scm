(use-modules
  (gnu)
  (channel-b packages suckless)
  )
(use-package-modules
  image-viewers
  suckless
  xdisorg
  xorg
  )

(packages->manifest
 (list
  ;; applications
  feh                                   ;for setting background
  sxhkd                                 ;hotkey daemon
  br-dmenu
  br-dwm                                ;window manager
  br-dwmblocks                          ;status bar
  br-st
  slock                                 ;screen locker
  ;; Xorg
  sx
  xf86-video-intel
  xset
  xclip
  xinput
  xwininfo
  ;; xorg-server
  ))
