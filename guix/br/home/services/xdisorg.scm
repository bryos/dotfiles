(define-module (br home services xdisorg)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:export (home-bemenu-service-type
            home-bemenu-configuration
            ))


;;; Commentary:
;;;
;;; This module contains xdisorg related services like Bemenu.
;;;
;;; Code:


;;;
;;; Bemenu
;;;

(define-configuration home-bemenu-configuration
  (package
   (package bemenu)
   "The bemenu package to use.")
  (font
   (pair '("Monospace" . 12))
   "Font to use in $BEMENU_OPTS, specified as @code{(font . size)}, with FONT
a string and SIZE a number.")
  (foreground
   (string "#FFFFFF")
   "Foreground color to use in $BEMENU_OPTS.")
  (background
   (string "#000000")
   "Background color to use in $BEMENU_OPTS.")
  (highlight-foreground
   (string "")
   "Highlighted foreground color to use in $BEMENU_OPTS. If this is an empty
string FOREGROUND will be used as highlighting foreground.")
  (highlight-background
   (string "#000000")
   "Highlighted background color to use in $BEMENU_OPTS.")
  (bemenu-opts
   (string "")
   "Value of the BEMENU_OPTS environment variable. See @code{man bemenu}
for details on how configure it.")
  (no-serialization))

(define (home-bemenu-profile-service config)
  (list (home-bemenu-configuration-package config)))

(define (home-bemenu-environment-variables-service config)
  (define (single-quote . ls)
    "Reture a single quoted string from the list LS, with the elements of LS
space seperated."
    (string-append "'" (string-join ls) "'"))

  (let* ((fg (home-bemenu-configuration-foreground config))
         (bg (home-bemenu-configuration-background config))
         (hf (home-bemenu-configuration-highlight-foreground config))
         (hf (if (string-null? hf)
                    fg
                    hf))
         (hb   (home-bemenu-configuration-highlight-background config))
         (font (single-quote (car (home-bemenu-configuration-font config))
                             (number->string
                              (cdr (home-bemenu-configuration-font config)))))
         (opts (home-bemenu-configuration-bemenu-opts config)))
    `(("BEMENU_OPTS" . ,(string-join (list
                                      "--hb" hb "--hf" hf
                                      "--tb" hb "--tf" hf
                                      "--ab" bg "--af" fg
                                      "--fb" bg "--ff" fg
                                      "--nb" bg "--nf" fg
                                      "--fn" font
                                      opts
                                      ))))))

(define home-bemenu-service-type
  (service-type (name 'home-bemenu)
                (extensions
                 (list (service-extension
                        home-profile-service-type
                        home-bemenu-profile-service)
                       (service-extension
                        home-environment-variables-service-type
                        home-bemenu-environment-variables-service)
                       ))
                (default-value (home-bemenu-configuration))
                (description "Install and configure @code{bemenu}")))
