(define-module (br home services password-utils)
  #:use-module (gnu)
  ;; #:use-module (gnu packages)
  #:use-module (guix packages)
  ;; #:use-module (guix profiles)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu home services)
  ;; #:use-module (gnu home services shells)
  #:use-module (gnu packages password-utils)
  #:export (home-password-store-service-type
            home-password-store-configuration
            ))


;;; Commentary:
;;;
;;; This module contains password related services like Password-Store.
;;;
;;; Code:


;;;
;;; Password-Store
;;;

(define-configuration home-password-store-configuration
  (package
   (package password-store)
   "The password-store package to use.")
  (extensions
   (list '())
   "List of password-store extensions that can be passed to
@code{specification->package}.")
  (password-store-dir
   (string "")
   "Value of the PASSWORD_STORE_DIR environment variable.")
  (no-serialization))

(define (home-password-store-profile-service config)
  (cons (home-password-store-configuration-package config)
        ;; (map specification->package
        ;;      (home-password-store-configuration-extensions config))))
        (home-password-store-configuration-extensions config)))

(define (home-password-store-environment-variables-service config)
  (list (cons
         "PASSWORD_STORE_DIR"
         (home-password-store-configuration-password-store-dir config))))

(define home-password-store-service-type
  (service-type (name 'home-password-store)
                (extensions
                 (list (service-extension
                        home-profile-service-type
                        home-password-store-profile-service)
                       (service-extension
                        home-environment-variables-service-type
                        home-password-store-environment-variables-service)
                       ))
                (default-value (home-password-store-configuration))
                (description "Install and configure @code{password-store}")))
