(define-module (br home services haskell-apps)
  #:use-module ((br home services utils) #:prefix br:)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services)
  #:use-module (gnu packages haskell-apps)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu)
  #:use-module (guix packages)
  #:export (home-kmonad-service-type
            home-kmonad-configuration
            ))

;;; TODO: shepherd service; kmonad requires root rights

;;; Commentary:
;;;
;;; This module contains kmonad related services.
;;;
;;; Code:


;;;
;;; Kmonad
;;;

(define-configuration home-kmonad-configuration
  (package
   (package kmonad)
   "The kmonad package to use.")
  (keyboard
   (string "")
   "Kmonad keyboard configuration file path that can be given to
@code{xdg-config-files-symlink-target} function, that can be found in
@file{br/home/services/utils.scm}.")
  (shepherd-autostart
   (boolean #f)
   "Autostart @code{kmonad} on login.")
  (no-serialization))

(define (home-kmonad-profile-service config)
  (list (home-kmonad-configuration-package config)))

(define (home-kmonad-config-files-service config)
  (list (br:xdg-config-files-symlink-target ;function from utils.scm
         (home-kmonad-configuration-keyboard config))))

(define (home-kmonad-shepherd-service config)
  (list
   (shepherd-service
    (provision '(kmonad))
    (documentation "Run and control kmonad.")
    ;; (one-shot? #t)
    (auto-start? #~(home-kmonad-configuration-shepherd-autostart? config))
    (start #~(make-forkexec-constructor
              `(,#$(file-append kmonad "/bin/kmonad")
                ,(in-vicinity (getenv "XDG_CONFIG_HOME")
                               #$(home-kmonad-configuration-keyboard config)))))
    (stop #~(make-kill-destructor)))))

(define home-kmonad-service-type
  (service-type (name 'home-kmonad)
                (extensions
                 (list (service-extension
                        home-profile-service-type
                        home-kmonad-profile-service)
                       (service-extension
                        home-xdg-configuration-files-service-type
                        home-kmonad-config-files-service)
                       ;; (service-extension
                       ;;  home-shepherd-service-type
                       ;;  home-kmonad-shepherd-service)
                       ))
                (default-value (home-kmonad-configuration))
                (description "Install and configure @code{kmonad}.")))
