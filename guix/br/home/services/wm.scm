(define-module (br home services wm)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu packages wm)
  #:use-module (gnu home services)
  #:use-module (gnu home services xdg)
  ;; #:use-module (ice-9 match)
  #:export (home-swaylock-service-type
            home-swaylock-configuration
            ))


;;;
;;; Swaylock
;;;

(define-configuration home-swaylock-configuration
  (ignore-empty-password?
   (boolean #t)
   "When an empty password is provided, do not validate it.")
  (no-unlock-indicator?
   (boolean #f)
   "Disable the unlock indicator.")
  (image
   (string "")
   "[[<output>]:]<path> Display the given image, optionally only on the given
output. Use -c to set a background color. If the path potentially contains a
':', prefix it with another ':' to prevent interpreting part of it as
<output>.")
  (disable-caps-lock-text?
   (boolean #f)
   "Disable the Caps Lock text.")
  (indicator-caps-lock?
   (boolean #f)
   "Show the current Caps Lock state also on the indicator.")
  (scaling
   (string "")
   "Image scaling mode: stretch, fill, fit, center, tile, solid_color. Use
solid_color to display only the background color, even if a background image
is specified.")
  (color
   (string "FFFFFF")
   "<rrggbb[aa]> Turn the screen into the given color instead of white. If
`image' is used, this sets the background of the image to the given
color. Defaults to white (FFFFFF).")
  (extra-content
   (alist '())
   "Extra content appended as-is to the Swaylock configuration file.  Run
@command{man swaylock} for more information about the configuration
options. Boolean option should be specified with `#t' and anything else as a
string.")
  (no-serialization))

;; from bash-serialize-aliases in home/services/shells.scm
;; does not work
;; (define (alist->config-string alist seperator)
;;    #~(string-append
;;     #$@(map
;;      (match-lambda
;;        ((key . #t)
;;         #~(string-append #$key "\n"))
;;        ((key . value)
;;         #~(string-append #$key seperator #$value "\n"))
;;        (else
;;         "")
;;      alist))))

;; convert ALIST into a string of "key=value\n..." with '=' being the
;; SEPERATOR.
(define (alist->config-string alist seperator)
  (apply string-append
         (map (lambda (elem)
                (let ((key (car elem))
                      (val (cdr elem)))
                  (cond
                   ((and (boolean? val)
                         val)
                    (string-append key "\n"))
                   ((and (string? val)
                         (not (string-null? val)))
                    (string-append key seperator val "\n"))
                   (else
                    ""))))
              alist)))

(define (generate-swaylock-file config)
  (string-append
   (if (home-swaylock-configuration-ignore-empty-password? config)
       "ignore-empty-password\n"
       "")
   (if (home-swaylock-configuration-no-unlock-indicator? config)
       "no-unlock-indicator\n"
       "")
   (let ((image-path (home-swaylock-configuration-image config))
         (image-scaling (home-swaylock-configuration-scaling config)))
     (if (not (string-null? image-path))
         (string-append "image=" image-path "\n"
                        ;; TODO: should throw error when scaling is not a valid option
                        "scaling=" image-scaling "\n")
         ""))
   (if (home-swaylock-configuration-disable-caps-lock-text? config)
       "disable-caps-lock-text\n"
       "")
   (if (home-swaylock-configuration-indicator-caps-lock? config)
       "indicator-caps-lock\n"
       "")
   "color=" (home-swaylock-configuration-color config) "\n"
   ;; TODO: add extra content
   (let ((extra-content (home-swaylock-configuration-extra-content config)))
     (if (not (null? extra-content))
         (alist->config-string extra-content "=")
         ""))
  ))

(define (home-swaylock-file-service config)
  `(("swaylock/config"
     ,(plain-file "config"
                  (generate-swaylock-file config)))))

(define (home-swaylock-profile-service config)
  (list swaylock))

(define home-swaylock-service-type
  (service-type (name 'home-swaylock)
                (extensions
                 (list (service-extension
                        home-profile-service-type
                        home-swaylock-profile-service)
                       (service-extension
                        home-xdg-configuration-files-service-type
                        home-swaylock-file-service)))
                (default-value (home-swaylock-configuration))
                (description "")))
