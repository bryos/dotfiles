(define-module (br home services shells)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  ;; #:use-module (gnu packages shells)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:export (home-multi-shell-aliases-service-type
            home-multi-shell-aliases-configuration

            %default-apt-aliases
            %default-guix-aliases
            %default-pacman-aliases
            ))


;;; Commentary:
;;;
;;; This module contains a service for configurating the aliases of
;;; bash/zsh and abbreviation of fish with the same values.
;;;
;;; Note: zsh is not yet working; apparently zsh does not have an
;;; aliases configuation field. TODO: Add it to ZSHRC as a `plain-file'
;;;
;;; TODO: add framework for extension so other apps (emacs) can also
;;; share "keybindings" with shells.
;;;
;;; Code:


;;;
;;; Multi-shell
;;;

(define %default-pacman-aliases
  '(("pms"  . "pacman -S")
    ("pmss" . "pacman -Ss")
    ("psyu" . "pacman -Syu")
    ("pmq"  . "pacman -Q")
    ("prns" . "pacman -Rns")))

(define %default-apt-aliases
  '(("a"    . "apt")
    ("ati"  . "apt install")
    ("atr"  . "apt remove")
    ("atu"  . "apt update")
    ("atuu" . "apt update && sudo apt upgrade")
    ("atar" . "apt autoremove")))

(define %default-guix-aliases
  '(("gi"  . "guix install ")
    ("gr"  . "guix remove ")
    ("gs"  . "guix search ")
    ("gsh" . "guix shell ")
    ("gp"  . "guix pull")
    ("gpl" . "guix package --list-installed ")
    ("ghr" . "guix home reconfigure $DOTFILE_DIR/guix/home.scm")
    ("gsr" . "sudo -E guix system reconfigure $DOTFILE_DIR/guix/system.scm")))

(define-configuration home-multi-shell-aliases-configuration
  (aliases
   (alist '())
   "Extra aliases, defined in the same manner as bash/zsh/fish aliases.")
  (extend-shells
   (list '())
   "List of shells that should be extended. This must be a list of
symbols. The symbols can be one or more of: @code{bash}, @code{zsh}
and @code{fish}. Note that @code{fish} will get abbreviations and not
aliases. This behaviour is hardcoded.")
  (prefix-sudo?
   (boolean #f)
   "Prefix all given aliases with the configuration field SUDO.")
  (sudo
   ;; TODO: also add this as a package to the profile.
   (string "sudo")
   "Program used for elevating priveledges, usually @code{sudo} or
@code{doas}.")
  (no-serialization))

(define (prefix-aliases-with-sudo-maybe config)
  (let ((aliases (home-multi-shell-aliases-configuration-aliases config)))
    (if (home-multi-shell-aliases-configuration-prefix-sudo? config)
        (map (lambda (alias-cons-pair)
               ;; prepend all alias values with sudo
               (cons (car alias-cons-pair)
                     (string-append (home-multi-shell-aliases-configuration-sudo config)
                                    " "
                                    (cdr alias-cons-pair))))
             aliases)
        aliases)))

(define (home-multi-shell-bash-service config)
  (let ((aliases (home-multi-shell-aliases-configuration-aliases config)))
    (if (and (member 'bash (home-multi-shell-aliases-configuration-extend-shells config))
             (not (null? aliases)))
        (home-bash-extension
         (aliases (prefix-aliases-with-sudo-maybe config)))
        (home-bash-extension))))

;; (define (home-multi-shell-zsh-service config)
;;   (home-zsh-extension
;;    (aliases (add-multi-shell-aliases config "zsh")))
;;   )

(define (home-multi-shell-fish-service config)
  (let ((aliases (home-multi-shell-aliases-configuration-aliases config)))
    (if (and (member 'fish (home-multi-shell-aliases-configuration-extend-shells config))
             (not (null? aliases)))
        (home-fish-extension
         (abbreviations (prefix-aliases-with-sudo-maybe config)))
        (home-fish-extension))))

(define home-multi-shell-aliases-service-type
  (service-type (name 'home-swaylock)
                (extensions
                 (list (service-extension
                        home-bash-service-type
                        home-multi-shell-bash-service)
                       ;; (service-extension
                       ;;  home-zsh-service-type
                       ;;  home-multi-shell-zsh-service)
                       (service-extension
                        home-fish-service-type
                        home-multi-shell-fish-service)
                       ))
                (default-value (home-multi-shell-aliases-configuration))
                (description "Configure multiple shells to have the same aliases.")))
