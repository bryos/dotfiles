(define-module (br home services emacs)
  #:use-module ((br home services utils) #:prefix br:)
  #:use-module (gnu home services shepherd)
  #:use-module (gnu home services)
  #:use-module (gnu packages emacs)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu)
  #:use-module (guix packages)
  #:export (home-emacs-service-type
            home-emacs-configuration
            ))


;;; Commentary:
;;;
;;; This module contains emacs related services.
;;;
;;; Code:


;;;
;;; Emacs
;;;

;;; well known files/directories that should always be added to the
;;; store. Use @code{#:recursive? #t} if it is a directory.
(define basic-emacs-files '(("init.el")
                            ("early-init.el")
                            ("yasnippets" #:recursive? #t)))

(define-configuration home-emacs-configuration
  (package
   (package emacs)
   "The emacs package to use.")
  (extra-files
   (text-config '())
   "List of file paths (as they can be found in @file{.config/emacs}) that can
be given to @code{xdg-config-files-symlink-target} function, that can be found
in @file{br/home/services/utils.scm}.")
  (extra-packages
   (list '())
   "3rd party dependencies to install with guix.")
  (editor
   (string "emacs")
   "Value of the EDITOR environment variable.")
  (no-serialization))

(define (home-emacs-profile-service config)
  (cons (home-emacs-configuration-package config)
        (home-emacs-configuration-extra-packages config)))

(define (home-emacs-environment-variables-service config)
  (let ((editor (home-emacs-configuration-editor config)))
    (if (string-null? editor)
        '()
        `(("EDITOR" . ,editor)))))

(define (home-emacs-config-files-service config)
  (map (lambda (file)
         (apply br:xdg-config-files-symlink-target ;function from utils.scm
                (cons (in-vicinity "emacs" (car file))
                      (cdr file))))
       (append (home-emacs-configuration-extra-files config)
               basic-emacs-files)))

(define (home-emacs-shepherd-service config)
  (list
   (shepherd-service
    (provision '(emacsd))
    (documentation "Run and control emacsclient.")
    (modules '((srfi srfi-1)
               (srfi srfi-26)))
    (one-shot? #t)
    (start #~(make-forkexec-constructor
              `(,#$(file-append emacs "/bin/emacs") "--daemon")
              ;; always use SHELL=/bin/bash inside emacs. Copied form
              ;; the redshift service type.
              #:environment-variables
              (cons (string-append "SHELL=/bin/bash")
                    (remove (cut string-prefix? "SHELL=" <>)
                            (default-environment-variables)))))
    (stop #~(make-system-destructor "emacsclient --eval '(kill-emacs)'")))))

(define home-emacs-service-type
  (service-type (name 'home-emacs)
                (extensions
                 (list (service-extension
                        home-profile-service-type
                        home-emacs-profile-service)
                       (service-extension
                        home-xdg-configuration-files-service-type
                        home-emacs-config-files-service)
                       (service-extension
                        home-environment-variables-service-type
                        home-emacs-environment-variables-service)
                       ;; shepherd service creates problems with environment
                       ;; variables.
                       ;; (service-extension
                       ;;  home-shepherd-service-type
                       ;;  home-emacs-shepherd-service)
                       ))
                (default-value (home-emacs-configuration))
                (description "\
Install and configure @code{emacs}. This service type assumes your
emacs config files live in @file{.config/emacs/}, relative to your
home configuration file.")))
