(define-module (br home services rust-apps)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu home services)
  #:use-module (gnu home services shells)
  #:export (home-exa-service-type
            home-exa-configuration
            ))


;;; Commentary:
;;;
;;; This module contains rust-app related services like Exa.
;;;
;;; TODO: when pacman.scm has configuration extension setup, use it.
;;;
;;; TODO: how to know which shells get aliases
;;;
;;; TODO: change package exa back to eza
;;;
;;; Code:


;;;
;;; Exa
;;;

(define-configuration home-exa-configuration
  (package
   (package exa) ;TODO change to eza
   "The exa package to use. The default is @code{eza}; the new community
fork of @code{exa}.")
  (aliases
   (alist '())
   "Set shell aliases, set it the same way as bash/fish aliases e.g.
@code{'((key . value) ...)}.")
  (extend-shells
   (list '())
   "List of shells that should be extended. This must be a list of
symbols. The symbols can be one or more of: @code{bash}, @code{zsh}
and @code{fish}.")
  (exa-colors
   (string "")
   "Value of the EXA_COLORS environment variable. See @code{man 3
exa_colors} for details on how configure it.")
  (no-serialization))

(define (home-exa-aliases-bash-service config)
  (let ((aliases (home-exa-configuration-aliases config)))
    (if (and (member 'bash (home-exa-configuration-extend-shells config))
             (not (null? aliases)))
        (home-bash-extension
         (aliases aliases))
        (home-bash-extension))))

;; (define (home-pacman-zsh-service config)
;;   (home-zsh-extension
;;    (aliases (add-pacman-aliases config "zsh")))
;;   )

(define (home-exa-aliases-fish-service config)
  (let ((aliases (home-exa-configuration-aliases config)))
    (if (and (member 'fish (home-exa-configuration-extend-shells config))
             (not (null? aliases)))
        (home-fish-extension
         (aliases aliases))
        (home-fish-extension))))

(define (home-exa-profile-service config)
  (list (home-exa-configuration-package config)))

(define (home-exa-environment-variables-service config)
  `(("EXA_COLORS" . ,(home-exa-configuration-exa-colors config))))

(define home-exa-service-type
  (service-type (name 'home-exa)
                (extensions
                 (list (service-extension
                        home-profile-service-type
                        home-exa-profile-service)
                       (service-extension
                        home-environment-variables-service-type
                        home-exa-environment-variables-service)
                       (service-extension
                        home-bash-service-type
                        home-exa-aliases-bash-service)
                       ;; (service-extension
                       ;;  home-zsh-service-type
                       ;;  home-pacman-zsh-service)
                       (service-extension
                        home-fish-service-type
                        home-exa-aliases-fish-service)
                       ))
                (default-value (home-exa-configuration))
                (description "Install and configure @code{exa}")))
