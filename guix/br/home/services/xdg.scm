(define-module (br home services xdg)
  #:use-module (gnu home services shells)
  #:use-module (gnu home services)
  #:use-module (gnu packages rust-apps)
  #:use-module (gnu services configuration)
  #:use-module (gnu services)
  #:use-module (gnu)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:export (home-force-xdg-specification-service-type
            home-force-xdg-specification-configuration

            home-xdg-user-directories-service-type
            home-xdg-user-directories-configuration

            %default-force-xdg-specification-environment-variables
            %extra-force-xdg-specification-environment-variables
            %extra-force-xdg-specification-aliases
            ))


;;; Commentary:
;;;
;;; This module contains service related to the xdg specification.
;;;
;;; TODO: when pacman.scm has configuration extension setup, use it.
;;;

;;;
;;; Code:


;;;
;;; Force-xdg-specification
;;;


;; Preferably this will become an exhaustive list of all known
;; environment variables for software that officially supports the xdg
;; specification. For a large but non-exhaustive list see:
;; https://wiki.archlinux.org/title/XDG_Base_Directory
(define %default-force-xdg-specification-environment-variables
  '(
    ("CABAL_CONFIG"       . "$XDG_CONFIG_HOME/cabal/config") ;Haskell
    ("CABAL_DIR"          . "$XDG_CACHE_HOME/cabal")         ;Haskell
    ("CARGO_HOME"         . "$XDG_DATA_HOME/cargo")          ;Rust
    ("DOCKER_CONFIG"      . "$XDG_CONFIG_HOME/docker")
    ("GHCUP_USE_XDG_DIRS" . "True")                          ;Haskell
    ;; TODO: wait for home-gpg-agent-service-type to respect the environment
    ;; variable.
    ;; ("GNUPGHOME" . "$XDG_DATA_HOME/gnupg")
    ("GTK2_RC_FILES"      . "$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0")
    ("GUILE_HISTORY"      . "$XDG_CACHE_HOME/guile/guile_history")
    ;; HISTFILE is also used by zsh
    ("HISTFILE"           . "$XDG_CACHE_HOME/bash/history")
    ("JUPYTER_CONFIG_DIR" . "$XDG_CONFIG_HOME/jupyter")
    ("NODE_REPL_HISTORY"  . "$XDG_DATA_HOME/node_repl_history")
    ("RUSTUP_HOME"        . "$XDG_DATA_HOME/rustup")
    ("STACK_XDG"          . "1")
    ("VIMINIT"            . "source $XDG_CONFIG_HOME/nvim/init.vim")
    ("WGETRC"             . "$XDG_CONFIG_HOME/wget/wgetrc")
    ("_JAVA_OPTIONS"      . "-Djava.util.prefs.userRoot=${XDG_CONFIG_HOME}/java")
    ))

;;; These variables will like not work if you dont use my exact setup.
(define %extra-force-xdg-specification-environment-variables
  '(
    ;; PYTHONSTARTUP is a personal script that can be found in my
    ;; dotfiles repo. If you want to use it you should place it
    ;; somewhere in your PATH and change the the value below to where
    ;; you placed it.
    ("PYTHONSTARTUP"      . "$HOME/.guix-profile/bin/PYTHONSTARTUP") ;to move .python_history
    ;; XAUTHORITY is going to break most display managers.
    ("XAUTHORITY"         . "$XDG_RUNTIME_DIR/Xauthority")
    ))

;; These aliases will also require some extra work to get working.
;; Often times it is just moving the file out of the HOME directory to
;; some XDG directory, the alias will likely point to the directory to
;; copy the file to. Each alias is append with the file it is trying
;; to move out of HOME.
(define %extra-force-xdg-specification-aliases
  '())

(define-configuration home-force-xdg-specification-configuration
  (aliases
   (alist '())
   "Set shell aliases, set it the same way as bash/fish aliases e.g.
@code{'((key . value) ...)}.")
  (extend-shells
   (list '())
   "A list of shells that should receive the aliases. The shells should be
symbols and be one or more of: bash, zsh, fish. So to set the aliases
for bash and fish use:

@example
(shells '(bash fish))
@end_example")
  (environment-variables
   (alist '())
   "Alist of environment variable that force applications to follow the
xdg desktop specification. Specify the environment variables the same
way as with @code{home-environment-variables-sevice-type} e.g.
@code{'((key . value) ...)")
  (no-serialization))

(define (home-force-xdg-specification-aliases-bash-service config)
  (when (member 'bash (home-force-xdg-specification-configuration-extend-shells config))
    (home-bash-extension
     (aliases (home-force-xdg-specification-configuration-aliases config)))))

;; (define (home-force-xdg-specification-aliases-zsh-extension-service config)
;;   (home-zsh-extension
;;    (aliases (add-pacman-aliases config "zsh")))
;;   )

;; TODO: maybe switch to abbreviations
(define (home-force-xdg-specification-aliases-fish-service config)
  (when (member 'fish (home-force-xdg-specification-configuration-extend-shells config))
    (home-fish-extension
     (abbreviations (home-force-xdg-specification-configuration-aliases config)))))

(define (home-force-xdg-specification-environment-variables-service config)
  (home-force-xdg-specification-configuration-environment-variables config))

(define home-force-xdg-specification-service-type
  (service-type (name 'home-exa)
                (extensions
                 (list (service-extension
                        home-environment-variables-service-type
                        home-force-xdg-specification-environment-variables-service)
                       (service-extension
                        home-bash-service-type
                        home-force-xdg-specification-aliases-bash-service)
                       ;; (service-extension
                       ;;  home-zsh-service-type
                       ;;  home-pacman-zsh-extension-service)
                       (service-extension
                        home-fish-service-type
                        home-force-xdg-specification-aliases-fish-service)
                       ))
                (default-value (home-force-xdg-specification-configuration))
                (description "Make applications follow the xdg specification by setting environment
variables and shell aliases.")))


;;;
;;; xdg user directories
;;;

;; Overwrite the default behaviour because I don't like it.
(define-configuration home-xdg-user-directories-configuration
  (desktop
   (string "$HOME/Desktop")
   "Default ``desktop'' directory, this is what you see on your
desktop when using a desktop environment,
e.g. GNOME (@pxref{XWindow,,,guix.info}).")
  (documents
   (string "$HOME/Documents")
   "Default directory to put documents like PDFs.")
  (download
   (string "$HOME/Downloads")
   "Default directory downloaded files, this is where your Web-broser
will put downloaded files in.")
  (music
   (string "$HOME/Music")
   "Default directory for audio files.")
  (pictures
   (string "$HOME/Pictures")
   "Default directory for pictures and images.")
  (publicshare
   (string "$HOME/Public")
   "Default directory for shared files, which can be accessed by other
users on local machine or via network.")
  (templates
   (string "$HOME/Templates")
   "Default directory for templates.  They can be used by graphical
file manager or other apps for creating new files with some
pre-populated content.")
  (videos
   (string "$HOME/Videos")
   "Default directory for videos.")
  (no-serialization))

(define (home-xdg-user-directories-environment-variables-service config)
  `(("XDG_DESKTOP_DIR"     . ,(home-xdg-user-directories-configuration-desktop config))
    ("XDG_DOCUMENTS_DIR"   . ,(home-xdg-user-directories-configuration-documents config))
    ("XDG_DOWNLOAD_DIR"    . ,(home-xdg-user-directories-configuration-download config))
    ("XDG_MUSIC_DIR"       . ,(home-xdg-user-directories-configuration-music config))
    ("XDG_PICTURES_DIR"    . ,(home-xdg-user-directories-configuration-pictures config))
    ("XDG_PUBLICSHARE_DIR" . ,(home-xdg-user-directories-configuration-publicshare config))
    ("XDG_TEMPLATES_DIR"   . ,(home-xdg-user-directories-configuration-templates config))
    ("XDG_VIDEOS_DIR"      . ,(home-xdg-user-directories-configuration-videos config))
    ))

(define home-xdg-user-directories-service-type
  (service-type (name 'home-xdg-user-directories)
                (extensions
                 (list (service-extension
                        home-environment-variables-service-type
                        home-xdg-user-directories-environment-variables-service)))
                (default-value (home-xdg-user-directories-configuration))
                (description "Configure XDG user directories.")))
