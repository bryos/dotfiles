;; services:
;; - home-fontconfig
;; - home-znc (irc client)
;; -
;; (use-modules (ice-9 match))
;; (use-modules (ice-9 textual-ports))
;; (define contents (call-with-input-file "/etc/os-release" get-string-all))
;; (define os (string-copy
;;             (car (filter
;;                   (lambda (l) (string-prefix? "ID=" l))
;;                   (string-split contents #\newline)))
;;             3))
;; (match os ("guix" (display "guix")) ("arch" (display "Arch\n")))

(use-modules
  ;; prefix my personal services, etc with 'br:' for clearity and to
  ;; prevent builtin service with the same name from breaking this
  ;; configuration
  ((br home services emacs) #:prefix br:)
  ((br home services haskell-apps) #:prefix br:)
  ((br home services mozilla) #:prefix br:)
  ((br home services password-utils) #:prefix br:)
  ((br home services rust-apps) #:prefix br:)
  ((br home services shells) #:prefix br:)
  ((br home services utils) #:prefix br:)
  ((br home services wm) #:prefix br:)
  ((br home services xdg) #:prefix br:)
  ((br home services xdisorg) #:prefix br:)
  ((channel-b packages admin) #:prefix br:)
  ((channel-b packages fonts) #:prefix br:)
  ((channel-b packages gnome-xyz) #:prefix br:)
  ((channel-b packages hunspell) #:prefix br:)
  ((channel-b packages password-utils) #:prefix br:)
  ((channel-b packages wallpapers) #:prefix br:)
  ((channel-b packages yambar) #:prefix br:)
  (gnu home services desktop)
  (gnu home services gnupg)
  (gnu home services guix)
  (gnu home services ssh)
  (gnu home services shells)
  (gnu home services xdg)
  (gnu home services)
  (gnu services)
  (gnu)
  (guix channels)
  (ice-9 rdelim)
  )
(use-package-modules
  emacs-xyz
  gnupg
  haskell-apps
  hunspell
  mail
  ncurses
  password-utils
  python
  rust-apps
  shells
  terminals
  vim
  virtualization
  wm
  xdisorg
  zig-xyz
  )

(define os
  (let ((line ""))
    (call-with-input-file "/etc/os-release"
      (lambda (port)
        (while (and (not (eof-object? line))
                    (not (string-prefix? "ID=" line)))
          (set! line (read-line port)))))
    (if (eof-object? line)
        (error "Cannot determine OS from /etc/os-release, check home.scm")
        (cadr (string-split line #\=)))))

(define dotfiles-directory
  (or (getenv "DOTFILE_DIR")
      (in-vicinity (getenv "HOME")
                   "my-projects/gitlab/dotfiles")))

(define br/shell-aliases
  '(
    ("zap" . "wmname LG3D && zaproxy")
    ;; Colorize grep output
    ("grep" . "grep --color=auto")
    ("egrep" . "egrep --color=auto")
    ("fgrep" . "fgrep --color=auto")
   ))

(define br/shell-abbreviations
  '(
    ;; * Quickly change directories
    (".." . "cd ../")
    ("..." . "cd ../../")
    ;; * Directories
    ;; ** Personal
    (".b" . "cd $MY_BIN_DIR/")
    (".p" . "cd $MY_PROJECTS_DIR/")
    (".df" . "cd $DOTFILE_DIR/")
    (".u" . "cd $MY_MAIN_USB/")
    (".v" . "cd $XDG_DOCUMENTS_DIR/vm-shared/")
    ;; ** XDG Directories
    (".c" . "cd $XDG_CONFIG_HOME/")
    (".dm" . "cd $XDG_DOCUMENTS_DIR/")
    (".dl" . "cd $XDG_DOWNLOAD_DIR/")
    (".m" . "cd $XDG_DOCUMENTS_DIR/media/")
    (".P" . "cd $XDG_PICTURES_DIR/")
    (".r" . "cd $XDG_DOCUMENTS_DIR/ru/")
    ;; * Edit Files
    ("d" . "doas")
    ("n" . "nvim")
    ("dn" . "doas nvim")
    ;; * Mount USB
    ("mtu" . "$SUDO mount -o uid=1000,fmask=113,dmask=002 /dev/sda")
    ("mtt" . "$SUDO umount /dev/sda")
    ;; * Commands With Extra Flags
    ;; ** Core Utils
    ("mvi" . "mv -i")
    ("cpi" . "cp -i")
    ("md"  . "mkdir -pv")
    ;; ** Yt-dlp Download Audio Only
    ("ytdl-audio" . "yt-dlp -x --audio-format mp3")
    ))

(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
 (packages (list
            ;; make service for window manager environment
            dunst
            foot
            fish-foreign-env            ;useful for sourcing guix profiles
            hunspell                    ;dependency of emacs
            hunspell-dict-en-us
            br:hunspell-dict-nl
            redshift-wayland
            river
            swaybg
            ;; sxhkd
            wl-clipboard
            br:yambar-wayland-minimal
            br:gruvbox-theme
            br:gruvbox-wallpapers
            br:font-mononoki-nerd
            br:askpass-menu

            neovim
            ncurses
            virt-manager

            python-wrapper              ; implicitly provides `python'
            ))

 ;; Below is the list of Home services.  To search for available
 ;; services, run 'guix home search KEYWORD' in a terminal.
 (services
  (list (simple-service
         'br/env-vars-service
         home-environment-variables-service-type
         `(("PATH" . "$HOME/.local/bin:$PATH")
           ;; * Scripting Variables
           ;; ** Directories
           ("MY_MAIN_USB"     . "$XDG_DOCUMENTS_DIR/mnt/verbatim")
           ("MY_PROJECTS_DIR" . "$HOME/my-projects")
           ("DOTFILE_DIR"     . "$MY_PROJECTS_DIR/gitlab/dotfiles")
           ("ORG_DIR"         . "$XDG_DOCUMENTS_DIR/org")
           ("BR_MAIL_DIR"     . "${XDG_DOCUMENTS_DIR}/mail")
           ;; ** Software
           ("BROWSER"  . "firefox")
           ("DMENU"    . "bemenu -i")
           ("SHELL"    . ,(file-append fish "/bin/fish"))
           ("SUDO"     . "sudo -A") ;replace with 'sudo -A' when using sudo
           ("TERMINAL" . "st")
           ;; ** Other
           ("MANPAGER"     . "nvim +Man!")
           ("SUDO_ASKPASS" . ,(file-append br:askpass-menu "/bin/askpass-${DMENU%% *}"))
           ("THEME"        . "dark")
           ("TERM"         . "xterm-256color") ;getting proper colors
           ;; ** Guix
           ;; ("GUIX_LOCPATH"       . "$HOME/.guix-profile/lib/locale") ;only necessary on foreign distro's
           ("GUIX_PACKAGE_PATH"     . "$DOTFILE_DIR/.local/bin/.guix/modules") ;for my-scripts
           ("GUIX_EXTRA_PROFILES"   . "$HOME/.guix-extra-profiles")
           ("GUILE_LOAD_PATH"       . "$DOTFILE_DIR/guix") ;personal home-services and packages
           ;; ("GUILE_WARN_DEPRECATED" . "detailed")
           ;; * Misc
           ("LEDGER_FILE" . "${XDG_DOCUMENTS_DIR}/02-area/finance/current.journal")
           ))
        ;; Custom service types
        (service br:home-bemenu-service-type
                 (br:home-bemenu-configuration
                  (foreground "#ebdbb2")
                  (background "#282828")
                  (highlight-background "#974609")
                  (font '("Mononoki Nerd Font" . 11))))
        (service br:home-emacs-service-type
                 (br:home-emacs-configuration
                  (extra-files '(("elfeed-dashboard.org")
                                 ("personal" #:recursive? #t)))
                  (extra-packages (list
                                   emacs-vterm
                                   emacs-pdf-tools
                                   isync
                                   mu
                                   ))
                  (editor "emacsclient --create-frame --alternate-editor=")))
        (service br:home-exa-service-type
                 (br:home-exa-configuration
                  (exa-colors (string-join
                               '("ur=38;5;220" "uw=38;5;160" "ux=38;5;76" "ue=38;5;76" ;permission bits
                                 "sn=38;5;76"  "sb=38;5;76"  ;file size
                                 "uu=38;5;220" "un=38;5;214" ;users
                                 "gu=38;5;220" "gn=38;5;214" ;groups
                                 "da=38;5;67"                ;date
                                 "di=38;5;148" "ex=38;5;76"  "ln=38;5;72"  "lp=38;5;148" ;file types
                                 ;; file type extensions
                                 "*.mp3=38;5;105")
                               ":"))
                  (extend-shells '(bash fish))
                  (aliases `(("ll" . ,(file-append eza
                                                   "/bin/eza"
                                                   " -alg"
                                                   " --color=always"
                                                   " --group-directories-first"))))))
        (service br:home-firefox-service-type
                 (br:home-firefox-configuration
                  (profile ".mozilla/firefox/ywwfrnjc.default")
                  (userchrome `(;; `../` little hack to get `profiles.ini` in the
                                ;; directory above the profile
                                ("../profiles.ini" . ,(in-vicinity dotfiles-directory
                                                                   ".mozilla/firefox/profiles.ini"))
                                ("user.js" . ,(in-vicinity dotfiles-directory
                                                           ".mozilla/firefox/user.js"))
                                ("chrome/userChrome.css"
                                 . ,(in-vicinity dotfiles-directory
                                                 ".mozilla/firefox/chrome/userChrome-hover.css"))))
                  (moz-enable-wayland #t)))
        (service br:home-kmonad-service-type
                 (br:home-kmonad-configuration
                  (keyboard "kmonad/colemak.kbd")))
        (service br:home-password-store-service-type
                 ;; TODO: should depend on gpg service type
                 (br:home-password-store-configuration
                  (package br:br-pass)  ;uses `bemenu` in `passmenu`
                  (extensions (list pass-otp))
                  (password-store-dir "${XDG_DOCUMENTS_DIR}/pass")))
        (service br:home-multi-shell-aliases-service-type
                 (br:home-multi-shell-aliases-configuration
                  (extend-shells '(bash fish))
                  (aliases br:%default-guix-aliases)))
        (service br:home-swaylock-service-type
                 (br:home-swaylock-configuration
                  (ignore-empty-password? #t)
                  (color "000000")
                  (scaling "fill")
                  (image (if (string= os "guix")
                             "$WALLPAPER_DIR/guix.jpg"
                             "$WALLPAPER_DIR/arch.jpg"))))
        (service br:home-force-xdg-specification-service-type
                 (br:home-force-xdg-specification-configuration
                  (extend-shells '(bash fish))
                  (environment-variables
                   (append
                    br:%default-force-xdg-specification-environment-variables
                    br:%extra-force-xdg-specification-environment-variables))
                  (aliases br:%extra-force-xdg-specification-aliases)))
        (service br:home-xdg-user-directories-service-type
                 (br:home-xdg-user-directories-configuration
                  (documents "$HOME/documents")
                  (desktop   "$XDG_DOCUMENTS_DIR")
                  (download  "$XDG_DOCUMENTS_DIR/downloads")
                  (music     "$XDG_DOCUMENTS_DIR/media/music")
                  (pictures  "$XDG_DOCUMENTS_DIR/media/pictures")
                  (videos    "$XDG_DOCUMENTS_DIR/media/videos")))
        ;; Builtin service types
        (service home-bash-service-type
                 (home-bash-configuration
                  ;; the defaults are included in my bash/bashrc
                  ;; without the aliases
                  (guix-defaults? #f)
                  (bashrc `(,(local-file (in-vicinity dotfiles-directory
                                                      ".config/bash/bashrc"))))
                  ;; not all abbreviations make sense as aliases
                  (aliases (append br/shell-aliases
                                   br/shell-abbreviations))
                  (bash-profile `(,(local-file (in-vicinity dotfiles-directory
                                                            ".config/bash/bash_profile"))))))
        (service home-fish-service-type
                 (home-fish-configuration
                  (config `(,(local-file (in-vicinity dotfiles-directory
                                                      ".config/fish/config.fish"))))
                  (environment-variables '(("THEME" . "dark")))
                  (aliases br/shell-aliases)
                  (abbreviations br/shell-abbreviations)))
        (service home-openssh-service-type
                 (home-openssh-configuration
                  (hosts
                   ;; global options are not allowed by guix so use variables
                   ;; to easily add them to each host.
                   (let ((preferred-authentications
                          (string-join `("  PreferredAuthentications"
                                         ,(string-join '("publickey"
                                                         "keyboard-interactive"
                                                         "password")
                                                       ",")))))
                     (list (openssh-host (name "gitlab.com") ;bryanrinders
                                         (identity-file "~/.ssh/gitlab.pub")
                                         ;; (identity-file "~/.ssh/id_ed25519")
                                         (extra-content preferred-authentications))
                           (openssh-host (name "github.com") ;BryanRi
                                         (identity-file "~/.ssh/github1.pub")
                                         ;; (identity-file "~/.ssh/id_rsa")
                                         (extra-content preferred-authentications))
                           (openssh-host (name "github.com-bryanrinders") ;bryanrinders
                                         (host-name "github.com")
                                         (user "git")
                                         (identity-file "~/.ssh/github2.pub")
                                         ;; (identity-file "~/.ssh/gh-qbay")
                                         (extra-content preferred-authentications))
                           (openssh-host (name "192.168.1.116")
                                         (identity-file "~/.ssh/id_ed25519")
                                         (extra-content preferred-authentications))
                           (openssh-host (name "192.168.1.129")
                                         (identity-file "~/.ssh/id_ed25519")
                                         (extra-content preferred-authentications))
                           )))))
        (service home-gpg-agent-service-type
                 (home-gpg-agent-configuration
                  (ssh-support? #t)
                  (pinentry-program
                   (file-append pinentry-bemenu "/bin/pinentry-bemenu"))
                  (default-cache-ttl (* 3 60 60))
                  (max-cache-ttl (* 12 60 60))
                  (default-cache-ttl-ssh (* 3 60 60))
                  (max-cache-ttl-ssh (* 12 60 60))
                  ))
        ;; redshift service does not seem to work on artix. Probably
        ;; due to conflicting init system. Also does not work on
        ;; wayland because it has X11 dependencies.
        ;; (service home-redshift-service-type
        ;;          (home-redshift-configuration
        ;;           (redshift redshift-wayland)
        ;;           (adjustment-method 'wayland)
        ;;           (location-provider 'manual)
        ;;           (latitude 52.5)
        ;;           (longitude 5.6)
        ;;           (dawn-time "6:00")
        ;;           (dusk-time "18:45")
        ;;           (daytime-temperature 4000 )
        ;;           (nighttime-temperature 1600)
        ;;           (extra-content "fade=1")))
        (simple-service 'br/home-channels-service
                        home-channels-service-type
                        (list
                         (channel
                          (name 'nonguix)
                          (url "https://gitlab.com/nonguix/nonguix")
                          ;; Enable signature verification:
                          (introduction
                           (make-channel-introduction
                            "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                            (openpgp-fingerprint
                             "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
                         (channel       ; channel for personal projects
                          (name 'channel-b)
                          (branch "testing")
                          (url "https://gitlab.com/bryanrinders/channel-b")
                          (introduction
                           (make-channel-introduction
                            "354f47cdf5e43ddb53e018bc4143126b29dfdc25"
                            (openpgp-fingerprint
                             "19E4 6AE1 E85C 23A9 62CB  C1F4 5A37 AFD5 5B64 F250"))))
                         ))
        ;; (service home-files-service-type
        ;;          ;; for some reason guix cannot find the .nix-channels file
        ;;          `((".nix-channels" ,(in-vicinity dotfiles-directory ".nix-channels"))
        ;;            ))
        (service home-xdg-configuration-files-service-type
                 (map (lambda (arg)
                        (apply br:xdg-config-files-symlink-target arg))
                      '(
                        ;; ("alacritty/alacritty.yml")
                        ;; ("alsa/xps-asound.state")
                        ;; create service with shepherd extension
                        ;; ("dunst/dunstrc")
                        ("foot/foot.ini")
                        ("git" #:recursive? #t)
                        ("gtk-2.0/gtkrc-2.0")
                        ("gtk-3.0/gtk.css")
                        ("gtk-3.0/settings.ini")
                        ("isyncrc")
                        ("mimeapps.list")
                        ("mpv/mpv.conf")
                        ("nix/nix.conf")
                        ("nixpkgs/config.nix")
                        ("nvim/colors/gruvbox.vim")
                        ("nvim/init.vim")
                        ;; ("nyxt" #:recursive? #t)
                        ("redshift.conf")
                        ("river/init" #:recursive? #t) ;must be executable
                        ("sxhkd/sxhkdrc")
                        ("wget/wgetrc")
                        ;; ("xorg/xinitrc")
                        ("yambar/config.yml")
                        ("zathura/zathurarc")
                        )))
        )))
