(use-modules
  (gnu)
  (guix packages)
  (nongnu packages linux)
  (nongnu system linux-initrd)
 )
;; (use-system-modules
;;   setuid
;;  )
(use-package-modules
  bash
  certs
  freedesktop
  gnome
  linux
  package-management
  pulseaudio
  shells
  ssh
  version-control
  vim
  wm
  )
(use-service-modules
  desktop
  dbus
  networking
  nix
  pm
  sound
  ssh
  sysctl
  virtualization
  xorg
  )

;; Allow members of the "video" group to change the screen brightness.
(define br/backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

(define br/decrypted-home-partition-label "homecrypt")

(define (br/usb-file-system fs-label mount-directory fs-type)
  "Return a `file-system' object with common options set. Used creating an
/etc/fstab entry for a usb with label `FS-LABEL' with mount point
`MOUNT-DIRECTORY' and `FS-TYPE' for the type of file-system on the usb.

 The mount directory is prefixed with: ${XDG_DOCUMENTS_DIR}/mnt. `FS-LABEL'
and `FS-TYPE' can be found with blkid."
  (file-system
    (mount-point (string-append (getenv "XDG_DOCUMENTS_DIR")
                                "/mnt"
                                mount-directory))
    (device (file-system-label fs-label))
    (type fs-type)
    (options (alist->file-system-options
              '("rw"
                "relatime"
                "nofail"
                "user"
                ("uid"   . "1000")
                ("fmask" . "113")
                ("dmask" . "002"))))
    (mount? #f)
    (mount-may-fail? #t)
    (create-mount-point? #t)))

(operating-system
  (kernel linux)
  (kernel-arguments (list (string-append "modprobe.blacklist="
                                         "pcspkr"  ; annoying beeping sound in tty
                                         ;; attempt to make sound work on xps 9700 (failed)
                                         ;; ",snd_hda_intel"
                                         ;; ",snd-hda-intel"
                                         ;; from %default-kernel-argmuents
                                         ",usbkbd"
                                         ",usbmouse"
                                         )
                          "quiet"
                          ))
  (firmware (list sof-firmware linux-firmware))
  (initrd microcode-initrd)
  (locale "en_US.utf8")
  (timezone "Europe/Amsterdam")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "xps-guix")

  ;; The list of user accounts ('root' is implicit).
  (users (cons* (user-account
                (name "br")
                (comment "br")
                (uid 1000)
                (group "br")            ; make user identical to an Arch user
                ;; (group "users")      ; default user group for guix
                (home-directory "/home/br")
                (supplementary-groups '(
                                        "audio"
                                        "input"
                                        "kvm"
                                        "libvirt"
                                        "lp"
                                        "netdev"
                                        "users"
                                        "video"
                                        "wheel"
                                        )))
                ;; add a fake user to hack the fish shell into /etc/shells;
                ;; the problem function: `etc-files' in gnu/system/shadow.scm
                ;; only detects user shells
               (user-account
                (name "fisher")
                (group "users")
                ;; (home-directory #f)
                (create-home-directory? #f)
                (shell (file-append fish "/bin/fish")))
               %base-user-accounts))
  (groups (cons (user-group
                 (name "br")
                 ;; give group id: 1000 to make dual booting with Arch and
                 ;; using the same home directory more convenient
                 (id 1000))
                %base-groups))
  (sudoers-file
   (plain-file "sudoers"
               (string-append "root ALL=(ALL:ALL) ALL\n"
                              "%wheel ALL=(ALL:ALL) NOPASSWD: ALL")))
  ;; (setuid-programs
  ;;  (cons (setuid-program
  ;;         (program (file-append ... "/bin/...")))
  ;;        %setuid-programs))
  (packages (cons*
             ;; alsa-ucm-conf
             bluez
             dash
             dconf                      ; needed by virt-manager
             git
             libinput
             neovim
             ;; nix
             ntfs-3g
             openssh
             pam-gnupg
             pulseaudio
             tlp
             ;; remove unwanted packages from %base-packages
             (filter (lambda (base-package)
                       (not (member (package-name base-package)
                                    '("mg" "nano" "nvi" ;editors
                                      "wireless-tools"  ;deprecated
                                      ))))
                     %base-packages)))

  ;; Below is the list of system services.  To search for available
  ;; services, run 'guix system search KEYWORD' in a terminal.
  (services (cons*
             (simple-service 'add-extra-hosts
                             hosts-service-type
                             (list (host "0.0.0.0" "youtube.com"
                                         '("www.youtube.com"))))
             (service alsa-service-type
                      (alsa-configuration
                       (pulseaudio? #t)))
             (service pulseaudio-service-type)
             (service bluetooth-service-type
                      (bluetooth-configuration
                       ;; setting to fix the error: Bluetooth headset
                       ;; unable to connect: org.bluez.Error.Failed
                       ;; br-connection-profile-unavailable
                       (auto-enable? #t)
                       (controller-mode 'bredr)
                       ))
             (service dbus-root-service-type
                      (dbus-configuration
                       (services (list
                                  dconf ; fix virt-manager not saving settings
                                  ))))
             ;; (service docker-service-type)
             (service elogind-service-type) ; required by `guix home'
             ;; For libvirt to work properly add the user to the 'kvm' and
             ;; 'libvirt' groups
             (service libvirt-service-type
                      (libvirt-configuration
                       (unix-sock-group "libvirt")
                       (tls-port "16555")))
             (service network-manager-service-type)
             (service ntp-service-type) ; synchronize the clock over a network
             ;; some setup is required to make nix work:
             ;; https://guix.gnu.org/manual/en/guix.html#index-nix_002dservice_002dtype
             ;; 1. add a channel and pull the latest package expression, e.g.:
             ;;   nix-channel --add https://nixos.org/channels/nixpkgs-unstable
             ;;   nix-channel --update
             ;; 2. Create a symlink to your profile and activate Nix profile:
             ;;   ln -s "/nix/var/nix/profiles/per-user/$USER/profile" ~/.nix-profile
             ;;   source /run/current-system/profile/etc/profile.d/nix.sh
             ;; 3. it might be necessary to create "/nix/var/nix/profiles/per-user/$USER" and
             ;;    "/nix/var/nix/gcroots/per-user/$USER" owned by $USER.
             ;; (service nix-service-type) ; the nix package manager
             ;; (service openssh-service-type)
;;              (simple-service 'my-etc-service etc-service-type
;;                              (list
;;                               ("polkit-1/localauthority/50-local.d/com.github.swhkd.pkexec.pkla"
;;                                (plain-file "com.github.swhkd.pkexec.pkla"
;;                                            "\
;; [Enable swhkd for wheel group users]
;; Identity=unix-group:wheel
;; Action=com.github.swhkd.pkexec
;; ResultActive=yes
;; "))))
             (service polkit-service-type)
             polkit-wheel-service
             (service screen-locker-service-type
                      (screen-locker-configuration
                       (name "swaylock")
                       (program (file-append swaylock "/bin/swaylock"))
                       (using-pam? #t)
                       (using-setuid? #f)))
             (service special-files-service-type
                      ;; /usr/bin/env is created by default
                      `(("/bin/bash" ,(file-append bash "/bin/bash"))
                        ("/bin/fish" ,(file-append fish "/bin/fish"))
                        ("/bin/sh"   ,(file-append dash "/bin/dash"))))
             (service tlp-service-type)
             ;; (service tor-service-type)
             (service udisks-service-type)
             (service wpa-supplicant-service-type)
             ;; for starting a X session from a tty with `sx` or `xinit`
             ;; https://mail.gnu.org/archive/html/help-guix/2021-08/msg00089.html
             (service xorg-server-service-type)
             (modify-services %base-services
               ;; use custom login service with pam-gpg enabled, defined in
               ;; pam-services
               (delete login-service-type)
               (udev-service-type config =>
                                  (udev-configuration
                                   (inherit config)
                                   (rules (cons br/backlight-udev-rule
                                                (udev-configuration-rules config)))))
               (guix-service-type config =>
                                  (guix-configuration
                                   (inherit config)
                                   (substitute-urls
                                    (cons "https://substitutes.nonguix.org"
                                          %default-substitute-urls))
                                   (authorized-keys
                                    (cons (local-file "./nonguix-key.pub")
                                          %default-authorized-guix-keys))))
               (sysctl-service-type config =>
                                    (sysctl-configuration
                                     (settings (append '( ; reduce console logging
                                                         ("kernel.printk" . "0 4 0 4"))
                                                       %default-sysctl-settings)))))))
  (pam-services (cons
                 ;; add pam-gpg to the default settings of
                 ;; `login-service-type'
                 (unix-pam-service "login"
                                   #:login-uid? #t
                                   #:allow-empty-passwords? #f
                                   #:motd "Hello!"
                                   #:gnupg? #t)
                 (base-pam-services)))
  (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (targets (list "/boot/efi"))
               (keyboard-layout keyboard-layout)
	       (timeout 10)
	       (menu-entries
		(list
		 (menu-entry
                  ;; Arch Linux grub entry using the same (encrypted)
                  ;; HOME partition as Guix
		  (label "Arch Linux")
		  (device (file-system-label "ROOT_ARCH"))
		  ;; (device (uuid "54a61e65-8eb2-472c-b647-ede54a87c521"
		  ;;       	'ext4))
		  (linux "/boot/vmlinuz-linux-lts")
		  (linux-arguments `(
				     "root=LABEL=ROOT_ARCH"
				     ;; "root=UUID=54a61e65-8eb2-472c-b647-ede54a87c521"
				     "rw"
				     "quiet"
				     ,(string-append "cryptdevice=UUID="
                                                     ;; TODO: give home partition a label
                                                     "4e7e0023-609d-4205-8dae-b60fd3d26522"
                                                     ":"
                                                     br/decrypted-home-partition-label)
				     ))
                  (initrd "/boot/initramfs-linux-lts.img"))
                 (menu-entry
                  (label "Do Not Touch!")
                  (device (uuid "3C8B-4C78" 'fat))
                  (chain-loader "/efi/Microsoft/Boot/bootmgfw.efi"))
                 ))))
  (mapped-devices (list (mapped-device
                         ;; TODO: give home partition a label
                         (source (uuid "4e7e0023-609d-4205-8dae-b60fd3d26522"))
                         (target br/decrypted-home-partition-label)
                         ;; (target "homecrypt")
                         (type luks-device-mapping))))
  (file-systems (cons* (file-system
                         (mount-point "/")
                         (device (file-system-label "ROOT_GUIX"))
                         ;; (device (uuid
                         ;;          "34bc522c-75cb-44d9-9405-dd4c625ec146"
                         ;;          'ext4))
                         (type "ext4"))
                       (file-system
                         (mount-point "/home")
                         (device (string-append "/dev/mapper/"
                                                br/decrypted-home-partition-label))
                         ;; (device "/dev/mapper/homecrypt")
                         (type "ext4")
                         (dependencies mapped-devices))
                       (file-system
                         (mount-point "/boot/efi")
                         (device (file-system-label "BOOT"))
                         ;; (device (uuid "3A62-21EF"
                         ;;               'fat32))
                         (type "vfat"))
                       ;; usb devices that will appear in /etc/fstab
                       (br/usb-file-system "Elements"    "/elements"  "ntfs")
                       (br/usb-file-system "Elements2"   "/elements2" "vfat")
                       (br/usb-file-system "KOBOeReader" "/ereader"   "vfat")
                       (br/usb-file-system "SANDISK_2"   "/sandisk2"  "vfat")
                       (br/usb-file-system "SANDISK_3"   "/sandisk3"  "vfat")
                       (br/usb-file-system "USB_SANDISK" "/sandisk"   "vfat")
                       (br/usb-file-system "VERBATIM"    "/verbatim"  "vfat")
		       %base-file-systems)))
