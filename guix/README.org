#+TITLE: README
#+SUBTITLE: Using My Guix Configuration
#+AUTHOR: Bryan Rinders
#+DATE: <2024-04-02>
#+OPTIONS: ^:{} toc:t num:t todo:t
#+PROPERTY: header-args :exports code :eval no-export

* Home
** First Reconfiguration
Running ~guix home reconfigure guix/home.scm~ for the first time will likely
result in the error:

#+begin_src text
guix home: error: failed to load 'guix/home.scm'
#+end_src

This is because it relies on guile modules defined in =./guix=. To fix this
you must set the ~GUILE_LOAD_PATH~ to the guix directory in this repo, e.g.:

#+begin_src sh
  export GUILE_LOAD_PATH="~/dotfiles/guix"
#+end_src

or add the load path to the ~reconfigure~ command. Assuming the current
directory is the root of this repo:

#+begin_src sh
  guix home -L ./guix guix/home.scm
#+end_src

After a successful reconfiguration, subsequent home reconfigurations can be
invoked with the ~bash~ and ~fish~ alias/abbreviation ~ghr~.

** Custom Home Services
Since Guix Home is still a fairly new project and therefore rather bare bones
I have defined some new services. For information on these home services go
[[file:br/home/services/README.org][here]].

* TODO System

* Extra Profiles
With guix you can use as many profiles as you want. A profile as a collection
of packages that can be install through a manifest file[fn:1]. This allows for
a very flexible setup, where you can pick and choose which manifests get
install on a particular machine, while keeping you work flow the same.

Below follow the steps for managing you manifest[fn:2].

1. Install and update profiles
   #+begin_src sh
     GUIX_EXTRA_PROFILES=~/.guix-extra-profiles
     mkdir -pv "${GUIX_EXTRA_PROFILES}"
     guix package -m /path/to/guix-my-project-manifest.scm -p "${GUIX_EXTRA_PROFILES}"/my-project/my-project
   #+end_src

2. Upgrade all profiles
   #+begin_src sh
     for profile in "${GUIX_EXTRA_PROFILES}"/*; do
       guix package --profile="$profile" --manifest="${DOTFILE_DIR}/guix/br/manifests/${profile}.scm"
     done
   #+end_src

3. Manually enable profiles
   #+begin_src sh
     GUIX_PROFILE="path/to/my-project" ; . "${GUIX_PROFILE}"/etc/profile
   #+end_src

4. Delete/switch generations

    You can do all ~guix package~ command for a profile all you have to do is
   specify the profile with the =-p= flag. For example to switch generations:

    #+begin_src sh
      guix package -p "${GUIX_EXTRA_PROFILES}"/my-project/my-project --switch-generations=5
    #+end_src

** Emacs
[[https://github.com/alezost/guix.el][Guix.el]] is an emacs package that can interact with the guix daemon and can
simplify managing your profiles. Read the manual to learn how to use it.

* Footnotes

[fn:1] [[info:Guix#Writing Manifests][info "(Guix) Writing Manifests"]]

[fn:2] [[info:Guix-cookbook#Guix Profiles in Practice][info "(Guix-cookbook) Guix Profiles in Practice"]] is a more detailed
guide on managing manifests.
