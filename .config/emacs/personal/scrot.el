;;; scrot.el --- screenshot utils -*- lexical-binding:t -*-

;; FIXME: if the IMAGE-NAME already exists, scrot will create a file
;; named IMAGE-NAME_000 and increments until a free name is found.
;;
;; TODO: make it screenshot program agnostic

;;;###autoload
(defun br/scrot ()
  "Take a screenshot with scrot and store the file location in
  the kill ring."
  (interactive)
  (let* ((image-name (completing-read "Enter image name w/o extension: " nil))
         (rename-file (concat "-F "
                              (if (string-equal "" image-name)
                                  (format-time-string "%Y-%m-%d-T%H%M%S.png")
                                (concat image-name ".png"))))
         (delay-p (string-equal "Yes" (completing-read "Add a delay? " '("No" "Yes")))))
    (when (eq 0 (shell-command (concat (when delay-p "sleep 3; ")
                                       "scrot -s "
                                       rename-file)))
      (if (directory-files default-directory t (format "^%s.png$" image-name))
          ;; find the real name of the image given by scrot
          (let ((index 0))
            (while (directory-files default-directory nil (format "^%s_%03d.png$"
                                                                  image-name
                                                                  index))
              (setq index (1+ index)))
            (setq image-name (format "%s_%03d"
                                     image-name
                                     index))))
      (setq image-name (concat image-name ".png"))
      (kill-new (let* ((absolute-path-to-image (concat default-directory image-name))
                       (link (concat "file:" absolute-path-to-image)))
                  (if (eq major-mode 'org-mode)
                      (org-link-make-string link image-name)
                    absolute-path-to-image)))))
  (message "File location stored in the kill ring."))

(provide 'scrot)

;;; scrot.el ends here
