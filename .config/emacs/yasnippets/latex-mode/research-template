# -*- mode: snippet -*-
# name: research-template
# key: rehead
# --
%% -*- usepackage-minted-p: ${1:nil}; -*-

\documentclass[a4paper]{article}

\usepackage[backend=biber, style=apa]{biblatex}
\addbibresource{ref.bib}

${2:% }\usepackage{minted}          % environment for adding code blocks
\usepackage{booktabs}        % nicer tables
\usepackage{multirow}

${3:% }\usepackage{tikz}           % environment for drawing things
\usepackage{float}           % for the 'H' positioning of figures
\usepackage{a4wide}          % To decrease the margins
\usepackage{graphicx}        % To deal with including pictures.
\usepackage{color}           % To add color.
\usepackage{enumerate}       % Extend the default environment
\usepackage{enumitem}
\usepackage{array}           % Extend the default environment
\usepackage{float}           % for the 'H' positioning of figures
\usepackage{listings}        % for the formatting of code blocks
\usepackage[american]{babel}
\usepackage{amsmath,amssymb} % better equations

\lstset{                     % for the boxing of code blocks
    basicstyle=\footnotesize,
    frame=single,
    breaklines=true,
    numbers=left,
    tabsize=2,
}

\newcommand{\CO}{\ensuremath{\mathcal{O}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{$4}

\author{Bryan Rinders, s1060340\\Radboud University}

\begin{document}
\maketitle

\section{Introduction} % Sketch the context of the work so that the
reader understand what they are reading.

\section{Method}


\subsection{Specification}
% Specify the task at hand, with the assignment text as your
% guideline. Describe what the software should do.


\subsection{Design}
% Specify here your software design. Specify the different classes and
% data structures you use, packages used for I/O and visualisation
% etc., and the pseudo code for existing algorithms that you will
% implement.


\subsection{Implementation} \label{implemenation}
% Give (only) the crucial part of the implementation here, preferably
% with line numbers, and explain what it does without literally
% repeating the code. Highlight important aspects and don’t repeat
% trivial translations from the pseudo-code. Also give the complexity
% and correctness analyses of the algorithms.


\subsection{Testing}
% Explain how you will test and experiment on the implementations in
% order to get a good idea of 1. their correct working and 2. the
% empirical results you are interested in. How you would interpret the
% results. Oftentimes the assignment will ask you for particular
% empirical questions that you can summarize here.

% %How did you test for:
% %- tesing for correctness of the algorithms
% %- test complexity

\section{Results}
% Write up the results in a clear and easy to follow way. Use a table
% and if meaningful, a graph, to summarize results. Give the raw facts
% here, not the interpretation of the facts.


\section{Discussion}

% In the discussion you interpret the results. What do you see and
% what does it mean


\section{Conclusion}

\section{Reflection}

% \section{Appendices}
% Add pictures

\printbibliography

\end{document}
