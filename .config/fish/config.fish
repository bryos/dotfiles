### Source fish aliases and variables ###
for file in fish_aliases fish_abbreviations theme-env.fish
    [ -f $XDG_CONFIG_HOME/fish/$file ] && . $XDG_CONFIG_HOME/fish/$file
end
for file in fenv.main.fish fenv.fish
    set -l function_file ~/.guix-home/profile/share/fish/functions/$file
    [ -f $function_file ] && . $function_file
end

### EXPORT ###
set fish_greeting           # Supresses fish's intro message
# To use gpg's pinentry-curses this must be set for every new terminal
set -x GPG_TTY (tty)

### SET EITHER EMACS MODE OR VI MODE ###
fish_default_key_bindings
#fish_vi_key_bindings

### Colorscheme ###
function set_theme --description "Change theme based on argument (light or dark) or $THEME."
    if begin; test "$THEME" = "light"; and ! string match -q 'st-*' "$TERM"; or test "$argv[1]" = "light"; end
        # light theme for all terminals except st
        set --global fish_color_autosuggestion       51699f
        set --global fish_color_error                cc0000   --bold
        set --global fish_color_normal               black
        set --global fish_color_operator             00636a   # param expansion *, ~ and $
        set --global fish_color_host                 6216a6
        set --global fish_color_user                 58b1cd
        # Colorscheme: Snow Day
        set --global fish_color_cancel                        --reverse
        set --global fish_color_command              164CC9   --bold
        set --global fish_color_comment              007B7B
        set --global fish_color_cwd                  green
        set --global fish_color_cwd_root             red
        set --global fish_color_end                  02BDBD
        set --global fish_color_escape               00a6b2
        set --global fish_color_history_current               --bold
        set --global fish_color_host_remote
        set --global fish_color_keyword
        set --global fish_color_match                         --background=brblue
        set --global fish_color_option
        set --global fish_color_param                4319CC
        set --global fish_color_quote                4C3499
        set --global fish_color_redirection          248E8E
        set --global fish_color_search_match         bryellow --background=brblack
        set --global fish_color_selection            white    --bold --background=brblack
        set --global fish_color_valid_path                    --underline
        set --global fish_pager_color_background
        set --global fish_pager_color_completion     normal
        set --global fish_pager_color_description    B3A06D
        set --global fish_pager_color_prefix         normal   --bold --underline
        set --global fish_pager_color_progress       brwhite  --background=cyan
        set --global fish_pager_color_secondary_background
        set --global fish_pager_color_secondary_completion
        set --global fish_pager_color_secondary_description
        set --global fish_pager_color_secondary_prefix
        set --global fish_pager_color_selected_background     --background=brblack
        set --global fish_pager_color_selected_completion
        set --global fish_pager_color_selected_description
        set --global fish_pager_color_selected_prefix
    else
        # dark theme for all terminal and also light for st
        set --global fish_color_command           89e5e5
        set --global fish_color_normal            89e5e5
        set --global fish_color_param             89e5e5      --bold
        set --global fish_color_autosuggestion    aaaaaa
        set --global fish_color_error             ff6c6b
        set --global fish_color_host              7f7fff
        set --global fish_color_user              ffd700
        # Colorscheme: Base16 Default Light
        set --global fish_color_cancel                        --reverse
        set --global fish_color_comment           f7ca88
        set --global fish_color_cwd               8ec87c
        set --global fish_color_cwd_root          red
        set --global fish_color_end               ba8baf
        set --global fish_color_escape            86c1b9
        set --global fish_color_history_current               --bold
        set --global fish_color_match             7cafc2
        set --global fish_color_operator          7cafc2
        set --global fish_color_quote             f7ca88
        set --global fish_color_redirection       brgreen
        set --global fish_color_search_match      bryellow    --background=brblack
        set --global fish_color_selection         white       --bold --background=brblack
        set --global fish_color_valid_path                    --underline
        set --global fish_pager_color_completion  normal
        set --global fish_pager_color_description B3A06D
        set --global fish_pager_color_prefix      normal      --bold --underline
        set --global fish_pager_color_progress    brwhite     --background=cyan
        set --global fish_pager_color_selected_background     --background=brblack
    end
end

set_theme ''

### PROMPT ###
function fish_guix_environment_prompt -d \
    "Add [env] tag to the prompt when inside a guix environment or nix shell."
    if [ x"$GUIX_ENVIRONMENT" != x ] || [ x"$IN_NIX_SHELL" != x ];
        printf "%s [env]" (set_color --bold "#d5c4a1")
    end
end
set -g fish_prompt_pwd_dir_length 9  # expand the char length of sub directories
set -g __fish_git_prompt_color "#fbf1c7"
set -g __fish_git_prompt_showcolorhints true
set -g __fish_git_prompt_color_suffix '--bold'
# for some reason making the prefix bold makes it a dark grey color
# set -g __fish_git_prompt_color_prefix '--bold'
function fish_prompt -d "Write out the prompt"
    printf '%s%s\n%s[%s%s%s@%s%s' \
        (set_color --bold $fish_color_cwd) (prompt_pwd) \
        (set_color --bold "#993299") \
        (set_color --bold $fish_color_user) $USER \
        (set_color --bold red) \
        (set_color --bold $fish_color_host) $hostname
    fish_guix_environment_prompt
    fish_git_prompt
    printf '%s]%s$%s ' \
        (set_color --bold "#993299") \
        (set_color --bold "#66b266") \
        (set_color $fish_color_normal)
end
# remove the vi mode prompt
function fish_mode_prompt; end


### FUNCTIONS ###
# Functions needed for !! and !$
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

# The bindings for !! and !$
if [ $fish_key_bindings = "fish_vi_key_bindings" ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Function for creating a backup file
# ex: bak file.txt
# result: copies file as file.txt.bak
function bak --argument filename
    cp "$filename"{,.bak}
end

function ex --description "Expand or extract bundled & compressed files"
    set --local ext (echo "$argv[1]" | awk -F. '{print $NF}')
    switch "$ext"
        case tar  # non-compressed, just bundled
            tar -xvf "$argv[1]"
        case gz
            if test (echo "$argv[1]" | awk -F. '{print $(NF-1)}') = tar # tar bundle compressed with gzip
                tar -zxvf "$argv[1]"
            else  # single gzip
                gunzip "$argv[1]"
            end
        case tgz  # same as tar.gz
            tar -zxvf "$argv[1]"
        case bz2  # tar compressed with bzip2
            tar -jxvf "$argv[1]"
        case rar
            unrar x "$argv[1]"
        case zip
            unzip "$argv[1]"
        case '*'
            echo "unknown extension '$ext'"
    end
end

# Useful function for fish to work with (emacs') Vterm
function vterm_printf;
    if begin; [  -n "$TMUX" ]  ; and  string match -q -r "screen|tmux" "$TERM"; end
        # tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$argv"
    else if string match -q -- "screen*" "$TERM"
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$argv"
    else
        printf "\e]%s\e\\" "$argv"
    end
end

# Allow directory and prompt tracking in Vterm
function vterm_prompt_end;
    vterm_printf '51;A'(whoami)'@'(echo $hostname)':'(pwd)
end
functions --copy fish_prompt vterm_old_fish_prompt
function fish_prompt --description 'Write out the prompt; do not replace this. Instead, put this at end of your file.'
    # Remove the trailing newline from the original prompt. This is done
    # using the string builtin from fish, but to make sure any escape codes
    # are correctly interpreted, use %b for printf.
    printf "%b" (string join "\n" (vterm_old_fish_prompt))
    vterm_prompt_end
end

# Enable direnv if installed
command -v direnv >/dev/null && direnv hook fish | source
