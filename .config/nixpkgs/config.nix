{ pkgs }:
let
  inherit (pkgs) lib buildEnv;

in {
  allowUnfree = true;

  # install packages with `nix-env -i br-desktop-all`
  packageOverrides = pkgs: {
    desktopAll = lib.lowPrio (buildEnv {
      name = "br-desktop-all";
      paths = with pkgs; [
        vscode
        librewolf
      ];
    });
  };
}
