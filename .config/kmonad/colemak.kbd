;; -*- mode: emacs-lisp -*-

(defcfg
  ;; For Linux
  input  (device-file "/dev/input/by-path/platform-i8042-serio-0-event-kbd")
  output (uinput-sink "My KMonad output"
                      ;; "/usr/bin/sleep 0.2; DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' /usr/bin/xset r rate 240 60"
                      )

  ;; Comment this is you want unhandled events not to be emitted
  fallthrough true

  ;; Set this to false to disable any command-execution in KMonad
  allow-cmd true
  )

;; US
(defsrc
  esc  mute vold volu                          prnt slck pause ins  home pgup
  f1   f2   f3   f4   f5   f6   f7   f8   f9   f10  f11  f12   del  end  pgdn
  grv  1    2    3    4    5    6    7    8    9    0    -     =    bspc
  tab  q    w    e    r    t    y    u    i    o    p    [     ]    \
  caps a    s    d    f    g    h    j    k    l    ;    '          ret
  lsft z    x    c    v    b    n    m    ,    .    /               rsft
  lctl lmet lalt           spc            ralt cmps rctl       back up   fwd
                                                               left down rght
  )

(defalias
  ;; layer toggles when holding down a key
  num (tap-hold-next-release 200 v (layer-toggle numbers))
  sym (tap-hold-next-release 200 m (layer-toggle symbols))
  arr (tap-hold-next-release 200 t (layer-toggle arrows))
  mse (tap-hold-next-release 200 s (layer-toggle mouse))
  ;; change regular key to a modifier when holding down a key
  bspa (tap-hold-next-release 200 bspc lalt)
  escs (tap-hold-next-release 200 esc lsft)
  actl (tap-hold-next-release 200 a lctrl)
  'ctl (tap-hold-next-release 200 ' lctrl)
  reta (tap-hold-next-release 200 ret lalt)
  spcm (tap-hold-next-release 200 spc lmet)
  ;; lead (multi-tap 200 @spcm C-x)  ;; leader key
  ;; switch base layers for when other people need to use my keyboard
  qwe (layer-switch qwerty)
  col (layer-switch colemak)
  )

(deflayer colemak
  esc   mute  vold volu                          prnt slck  pause @qwe home pgup
  f1    f2    f3   f4   f5   f6   f7   f8   f9   f10  f11   f12   del  end  pgdn
  grv   1     2    3    4    5    6    7    8    9    0     -     =    bspc
  tab   q     w    f    p    g    j    l    u    y    ;     [     ]    \
  @bspa @actl r    @mse @arr d    h    n    e    i    o     @'ctl @reta
  @escs z     x    c    @num b    k    @sym ,    .    /                rsft
  lctl  lmet  lalt           @spcm          ralt cmps rctl        pgdn up   pgup
                                                                  left down rght
  )

;; standard qwerty layer, switch back to colemak by pressing inssert (@col) on the
;; top right. pgup and pgdn are moved to the arrow keys cluster.
(deflayer qwerty
  esc  mute vold volu                          prnt slck pause @col home pgup
  f1   f2   f3   f4   f5   f6   f7   f8   f9   f10  f11  f12   del  end  pgdn
  grv  1    2    3    4    5    6    7    8    9    0    -     =    bspc
  tab  q    w    e    r    t    y    u    i    o    p    [     ]    \
  esc  a    s    d    f    g    h    j    k    l    ;    '          ret
  lsft z    x    c    v    b    n    m    ,    .    /               rsft
  lctl lmet lalt           spc            ralt cmps rctl       pgdn up   pgup
                                                               left down rght
  )

(deflayer numbers
  _    _    _    _                             _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    7    8    9    +    _    _    _
  _    _    _    lsft _    _    0    4    5    6    *    =         _
  _    _    ,    .    _    _    -    1    2    3    /              _
  _    _    _              _              _    _    _         _    _    _
                                                              _    _    _
  )

(deflayer symbols
  _    _    _    _                             _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  `    !    @    {    }    \    _    _    _    _    _    _    _    _
  _    #    $    \(   \)   &    _    _    _    _    _    _         _
  _    %    ^    [    ]    ~    _    _    _    _    _              _
  _    _    _              _              _    _    _         _    _    _
                                                              _    _    _
  )

(deflayer arrows
  _    _    _    _                             _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    lalt lctl lsft _    _    left down up   rght _    _         _
  _    _    _    _    _    _    _    _    _    _    _              _
  _    _    _              _              _    _    _         _    _    _
                                                              _    _    _
  )

;; not the most elegant way to move the mouse with the keyboard but it works.
(defalias
  mup (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative -- 0 -15")
  mdn (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative 0 15")
  mlt (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative 15 0")
  mrt (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative -- -15 0")
  mfu (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative -- 0 -150")
  mfd (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative 0 150")
  mfl (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative 150 0")
  mfr (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool mousemove_relative -- -150 0")
  mcl (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool click 1")
  mcr (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool click 3")
  )
;; better mouse movement
;; requires version 0.4.2
;;(defalias
;;  mup (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' while true; do xdotool mousemove_relative -- 0 -15 && sleep 0.2; done"
;;                  "killall xdotool")
;;  mdn (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' while true; do xdotool mousemove_relative 0 15 && sleep 0.2; done"
;;                  "killall xdotool")
;;  mlt (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' while true; do xdotool mousemove_relative 15 0 && sleep 0.2; done"
;;                  "killall xdotool")
;;  mrt (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' while true; do xdotool mousemove_relative -- -15 0 && sleep 0.2; done"
;;                  "killall xdotool")
;;  mcl (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool click 1")
;;  mcr (cmd-button "DISPLAY=':0' XAUTHORITY='/home/br/.config/X11/Xauthority' xdotool click 3")
;;  )

(deflayer mouse
  _    _    _    _                             _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    @mcl @mcr _    _    _    _    _
  _    _    _    _    _    _    @mrt @mdn @mup @mlt _    _         _
  _    _    _    _    _    _    @mfr @mfd @mfu @mfl _              _
  _    _    _              _              _    _    _         _    _    _
                                                              _    _    _
  )

#|
(deflayer empty
  _    _    _    _                             _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _    _    _
  _    _    _    _    _    _    _    _    _    _    _    _         _
  _    _    _    _    _    _    _    _    _    _    _              _
  _    _    _              _              _    _    _         _    _    _
                                                              _    _    _
  )
|#
;; vim: syntax=clojure
