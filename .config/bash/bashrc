#!/bin/bash
### ~/.bashrc

# if using bash configs in the .config directory then add the following line to /etc/bash.bashrc
# [ -s "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bashrc" ] && . "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bashrc"

if [[ $- != *i* ]]; then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile
    return
fi

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bash_aliases" ] \
    && . "${XDG_CONFIG_HOME:-$HOME/.config}/bash/bash_aliases"
[ -f "$HOME/.bash_aliases" ] && . "$HOME/.bash_aliases"

# needed for bash_history
[ -d "${XDG_STATE_HOME}/bash" ] || mkdir -p "${XDG_STATE_HOME}/bash"

# To use gpg's pinentry-curses this must be set for every new terminal
export GPG_TTY=$(tty)

### For setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

### SET VI MODE
# Comment this line out to enable default emacs-like bindings
# set -o vi

### PROMPT
if [ "${TERM}" = 'dumb' ]; then
    PS1='$ '  # for use with emacs' tramp
else
    # The ANSI escape sequences must be surrounded with `\[...\]` or
    # you will get problems scrolling through the bash history
    PS1='\[\e[0;36m\]\w\[\e[0m\]\n\[\e[0;33m\][\u@\h]\[\e[0;36m\]\$\[\e[0m\] '
fi

### SHOPT
shopt -s globstar   # recursive pathname expansion
shopt -s autocd     # change to named directory
shopt -s cdspell    # autocorrects cd misspellings
shopt -s cmdhist    # save multi-line commands in history as single line
#shopt -s dotglob
shopt -s histappend     # do not overwrite history
shopt -s checkwinsize   # checks term size when bash regains control

### Ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

### ARCHIVE EXTRACTION
# usage: ex <file>
ex () {
    if [ -f "$1" ] ; then
        case "$1" in
            *.tar.bz2)  tar xjf "$1"    ;;
            *.tar.gz)   tar xzf "$1"    ;;
            *.bz2)      bunzip2 "$1"    ;;
            *.rar)      unrar x "$1"    ;;
            *.gz)       gunzip "$1"     ;;
            *.tar)      tar xf "$1"     ;;
            *.tbz2)     tar xjf "$1"    ;;
            *.tgz)      tar xzf "$1"    ;;
            *.zip)      unzip "$1"      ;;
            *.Z)        uncompress "$1" ;;
            *.7z)       7z x "$1"       ;;
            *.deb)      ar x "$1"       ;;
            *.tar.xz)   tar xf "$1"     ;;
            *.tar.zst)  unzstd "$1"     ;;
            *)          echo "'$1' cannot be extracted via ex()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}
