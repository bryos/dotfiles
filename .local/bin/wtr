#!/usr/bin/guile \
-e main -s
!#

;;; Description:
;;; Display weather information.
;;;
;;; Dependencies:
;;; - guile
;;; - guile-gnutls
;;; - guile-json
;;;
;;; Notes: guile-gnutls requires some setup work
;;; https://gnutls.org/manual/gnutls-guile.html#Guile-Preparations
;;;
;;; Weather data source:
;;; https://www.buienradar.nl/overbuienradar/gratis-weerdata

(use-modules
 (json)                                 ;external library
 (rnrs bytevectors)
 (ice-9 getopt-long)
 (srfi srfi-19)
 (web client))


;;; Usage
(define (usage)
  (display "\
Usage: wtr [--help|--radar]
Display weather information. If no options are specified weather
information is displayed on the terminal.

    -h, --help  display this help and exit
    -r, --radar use `mpv` to show a rain radar"))


;;; Json types
(define-json-type <stationmeasurements>
  (stationname)
  (weatherdescription)
  (winddirection)
  (temperature)
  (precipitation))

(define-json-type <actual>
  (stationmeasurements "stationmeasurements" #(<stationmeasurements)))

(define-json-type <fivedayforecast>
  (day)
  (mintemperatureMin)
  (maxtemperatureMax)
  (mmRainMax)
  (windDirection))

(define-json-type <forecast>
  (fivedayforecast "fivedayforecast" #(<fivedayforecast)))

(define-json-type <weather>
  (actual "actual" <actual>)
  (forecast "forecast" <forecast>))


;;; Helper functions
(define (weather-day->week-day time-string)
  "Return the week day (e.g. Sun for sunday) of TIME-STRING. TIME-STRING
must look like '2024-04-19T00:00:00'."
  (date->string (string->date time-string "~Y-~m-~dT~H:~M:~S") "~a"))


;;; Pretty printing
(define (color-temperature temp)
  "Return TEMP-STRING with a foreground color matching the temperature it
represents."
  (let* ((escape (string #\esc #\[))
         (color (cond
                 ((< temp -5) 159)
                 ((< temp  0) 123)
                 ((< temp  5) 120)
                 ((< temp 10) 154)
                 ((< temp 15) 220)
                 ((< temp 20) 215)
                 ((< temp 25) 208)
                 ((< temp 30) 202)
                 ((< temp 35) 196)
                 ((< temp 40) 160)
                 (else 124))))
    ;; color needs a minwitdh of 3 (padded with spaces) to prevent
    ;; formatting issues in `pp-weather-forecast'
    (format #f "~a38;5;~3dm~f~0@*~a0;0m" escape color temp)))

(define (pp-weather-today today)
  "Pretty print the 'actual' weather of today."
  (format (current-output-port)
          "Weather today:\n~a\nTemp: ~a\nPrecipitation: ~f\nWind direction: ~a\n"
          (stationmeasurements-weatherdescription today)
          (color-temperature
           (stationmeasurements-temperature today))
          (stationmeasurements-precipitation today)
          (stationmeasurements-winddirection today)))

(define (pp-weather-forecast forecast)
  "Pretty print the 'fivedayforecast'."
  (display "\nFive day forecast:\nDay\tminTemp\tmaxTemp\tmmRain\twindDirection\n")
  (for-each
   (lambda (day)
     (format (current-output-port)
             "~a\t~22@a\t~22@a\t~3@a\t~4@a\n"
             (weather-day->week-day (fivedayforecast-day day))
             (color-temperature
              (fivedayforecast-mintemperatureMin day))
             (color-temperature
              (fivedayforecast-maxtemperatureMax day))
             (fivedayforecast-mmRainMax day)
             (fivedayforecast-windDirection day)))
   forecast))

(define (wtr)
  (define-values (header body)
    (http-get "https://data.buienradar.nl/2.0/feed/json"))
  (let* ((json-string (utf8->string body))
         (weather (json->weather json-string))
         (forecast (forecast-fivedayforecast (weather-forecast weather)))
         (groenlo? (lambda (station)
                           (string= (stationmeasurements-stationname station)
                                    "Meetstation Groenlo-Hupsel")))
         (today (car (filter groenlo? (actual-stationmeasurements (weather-actual weather))))))
    (pp-weather-today today)
    (pp-weather-forecast forecast)))

(define (radar)
  (let ((url "https://image.buienradar.nl/2.0/image/animation/RadarMapRainWebMercatorNL?")
        (render-bg "renderBackground=True")
        (render-text "renderText=True")
        ;; optional settings
        (render-branding "renderBranding=True")
        (width "256")
        (height "256"))
    (system* "mpv" "--loop=inf"
             (string-join (list url render-bg render-text) "&"))))


;;; Main
(define (main args)
  (let* ((option-spec '((help (single-char #\h) (value #f))
                        (radar (single-char #\r) (value #f))))
         (options (getopt-long args option-spec))
         (help? (option-ref options 'help #f))
         (radar? (option-ref options 'radar #f)))
    (cond (help? (usage))
          (radar? (radar))
          (else (wtr)))))
