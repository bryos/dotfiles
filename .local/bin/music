#!/usr/bin/env sh

# Description:
# Manage your music. Pick the genre to play or download music with
# yt-dlp.
#
# Dependencies:
# - mpv
# - yt-dlp
# - dmenu (like)

: "${XDG_MUSIC_DIR:=$HOME/Music}" \
  "${DMENU:=dmenu}"

usage() {
    printf "Usage %s [ option ]
Options:
    update-all, update the all directory with with symlinks to every mp3 file in %s.
    play,       use dmenu to choose what genre to play.
    URL,        use yt-dlp to download URL.
" "$0" "${XDG_MUSIC_DIR}"
}

case "$1" in
    u|update-all)
        find "${XDG_MUSIC_DIR}" \
             -type f \
             -name "*.mp3" \
             -exec ln -sv -t "${XDG_MUSIC_DIR}/all/" '{}' \+ ;;
    p|play)
        genre=$(find "${XDG_MUSIC_DIR}" -type d | sort | ${DMENU} -p "Which genre to play?" -l 20 | sed 's/^/--playlist=/') || exit 1
        # shellcheck disable=SC2086
        mpv --loop-playlist=inf --shuffle ${genre} & #> /dev/null 2>&1 &
        ;;
    *youtube.com/watch*)
        url_id="$(echo "$1" | cut -d = -f 2)"

        # yt-dlp -F "$1" \
        #     | awk '!/(^\[.*\])|(storyboard)|(video only)/ { print $0 } END { print "best" }' \
        #     | { ${DMENU} -p 'Choose a quality and format: ' -l 15 || exit; } \
        #     | cut -f 1 -d ' ' \
        #     | xargs -I {} yt-dlp -f {} "$1" \
        yt-dlp -x --audio-format mp3 "$1" \
            && find "$(pwd)" \
                    -type f \
                    -name "*\[${url_id}\].mp3" \
                    -exec ln -vs '{}' "${XDG_MUSIC_DIR}/all/" \; \
                    -exec ln -vs '{}' "${XDG_MUSIC_DIR}/new/" \;
        ;;
    *) usage; exit 1
esac
exit
