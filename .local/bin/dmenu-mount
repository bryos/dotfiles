#!/usr/bin/guile \
-e main -s
!#

;;; Description:
;;; Mount block devices (usb's) using a dmenu like application. The
;;; dmenu application is chosen from the environment variable $DMENU
;;; and defaults to `dmenu`.
;;;
;;; Dependencies:
;;; - br-utils (see ./guile/modules/br-utils.scm)
;;; - dmenu (like application e.g. bemenu)
;;; - Guile Scheme
;;; - guile-json
;;; - libnotify (optional)


;; FIX usb's with Elements label must also run:
;; `udisksctl power-off -b <path>`

(use-modules
  (br-utils)
  (json)
  (ice-9 format)
  (ice-9 getopt-long)
  (ice-9 popen)
  (ice-9 textual-ports))


;;; Usage
(define (usage)
  (format (current-error-port)
          "\
Usage: dmenu-mount -m|-u|-h
    -h, Display help text and exit.
    -m, Mounting a usb device chosen with `~a`.
    -u, Unmounting a usb device chosen with ~@*`~a`."
          dmenu))


;;; Helper functions
(define (device-paths device-string-list)
  "Returns the path to the device name from the DEVICE-STRING."
  ;; exit when no devices where selected
  (unless (list? device-string-list)
    (exit 1))
  (map (lambda (device-string)
         (string-append "/dev/" (car (string-split device-string #\ ))))
       device-string-list))


;;; Json definitions
(define-json-type <block-device>
  (name)
  (label)
  (size)
  (mountpoints)
  (children "children" #(<block-device)))

(define-json-type <block-devices>
  (blockdevices "blockdevices" #(<block-device)))


;;; Convert block-devices to strings
(define (block-device-field->string field-name)
  (cond ((symbol? field-name)
         (symbol->string field-name))
        (else
         field-name)))

(define (block-device-name->string device)
  "Return the name of DEVICE as a string."
  (block-device-field->string (block-device-name device)))

(define (block-device-label->string device)
  "Return the label of DEVICE as a string."
  (string-append
   "["
   (block-device-field->string (block-device-label device))
   "]"))

(define (block-device-size->string device)
  "Return the size of DEVICE as a string."
  (string-append
   "("
   (block-device-field->string (block-device-size device))
   ")"))

(define (block-device->string device)
  "Return the name, label and size of the block device as a space
 seperated string."
  (string-join
   (list
    (block-device-name->string device)
    (block-device-label->string device)
    (block-device-size->string device))))

;; TODO: maybe remove the top level disks and use only partitions. But
;; check if disk has children, then dont exclude it (for kobo
;; ereader). This requires adding TYPE to the `lsblk` options.
;;
;; TODO: maybe not necessary to check children since `filter-devices`
;; already returns a list of devices that have no children.
(define (block-devices->string-list lst)
  "Return a list of strings that can be used to echo it into dmenu"
  (cond ((null? lst) '())
        ((list? (block-device-children (car lst)))
         (append (list (block-device->string (car lst)))
                 (block-devices->string-list (block-device-children (car lst)))
                 (block-devices->string-list (cdr lst))))
        (else
         (cons (block-device->string (car lst))
               (block-devices->string-list (cdr lst))))))


;; Filter on block device having mount points or not depending on
;; whether to mount or umount
(define (block-device-unmounted? device)
  "Return #t if DEVICE is *not* mounted, #f otherwise."
  (array-equal? #(null) (block-device-mountpoints device)))

(define (block-device-mounted? device)
  "Return #t if DEVICE is mounted, #f otherwise."
  (not (block-device-unmounted? device)))

(define (filter-devices pred device-list)
  "Like `filter', but recursively check the children of DEVICE-LIST. Only
returns the device itself for which PRED returns #t and not the
parent. So non of the devices in the returned list will have children."
  (cond ((null? device-list) '())
        ((list? (block-device-children (car device-list)))
         (append (filter-devices pred (block-device-children (car device-list)))
                 (filter-devices pred (cdr device-list))))
        (else
         (let ((device (car device-list)))
           (append (if (pred device)
                       (list device)
                       '())
                   (filter-devices pred (cdr device-list)))))))


;;; Use notify-send to notify the user
(define (notify-pp exit-status device-string cmd)
  ;; exit-status   := 0|1, return value of system cmd
  ;; device-string := the (un)mounted device
  ;; cmd           := [u]mount
  (notify-send (format #f "~aing ~a" cmd device-string)
               ""                       ;TODO supply error message
               #:critical? (not (= 0 exit-status))))


;;; Mount or umount device(s)
(define (-mount device-path)
  "Try mounting DEVICE-PATH with /etc/fstab, if failed ask for
mountpoint and try again."
  (let* ((mount-cmd "mount")
         ;; do not use sudo when mounting with /etc/fstab, all usb
         ;; entries can be mounted by the user
         (exit-status (system* mount-cmd device-path)))
    ;; FIX if exit code is 32 then try mounting with sudo
    (unless (= 0 exit-status)
      (let* ((mountpoint (dmenu-choose-dir
                          (format #f "Where to mount '~a'?" device-path)
                          1
                          (in-vicinity (getenv "XDG_DOCUMENTS_DIR") "mnt")
                          "/mnt"))
             (mountoptions "uid=1000,fmask=113,dmask=002"))
        (set! exit-status (system* "sudo" "-A" mount-cmd device-path "-o" mountoptions mountpoint))))
    exit-status))

(define (mount device-paths)
  "Mount all device paths in DEVICE-PATHS and use `notify-send` to signal
succes/failure."
  (for-each
   (lambda (device-path)
     (let ((exit-status (-mount device-path)))
       (notify-pp exit-status device-path "mount")))
   device-paths))

(define (umount device-paths)
  "Unmount all the device paths in DEVICE-PATHS."
  (let ((umount "umount"))
    (for-each
     (lambda (device-path)
       (let ((exit-status (system* "sudo" "-A" umount device-path)))
         (notify-pp exit-status device-path umount)))
     device-paths)))


;;; Main
(define (main args)
  (let* ((option-spec '((help (single-char #\h) (value #f))
                        (mount (single-char #\m) (value #f))
                        (umount (single-char #\u) (value #f))))
         (options (getopt-long args option-spec))
         (mount? (option-ref options 'mount #f))
         (umount? (option-ref options 'umount #f))
         ;; -
         (lsblk (shell-eval->string "lsblk --json -o NAME,LABEL,SIZE,MOUNTPOINTS"))
         ;; FIX `lsblk' can be #f
         (block-devices (block-devices-blockdevices (json->block-devices lsblk)))
         (choices (lambda (pred)
                    (device-paths
                     (dmenu-choose-from
                      (block-devices->string-list
                       (filter-devices pred block-devices)))))))
    (cond ((and mount? umount?)
           (format (current-error-port)
                   "~a: You can not use -m and -u simultaneously.\nTry '~a --help' for more information."
                   (car args) (car args)))
          (mount? (mount (choices block-device-unmounted?)))
          (umount? (umount (choices block-device-mounted?)))
          (else (usage)))))
