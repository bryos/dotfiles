;;; Description:
;;; Utility functions
;;;
;;; Dependencies:
;;; - guile

(define-module (br-utils)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 textual-ports)
  #:export (
            dmenu
            dmenu-choose-dir
            dmenu-choose-from
            dmenu-mkdir
            dmenu-noyes
            notify-send
            shell-eval->string
            ))

;;; Global variables
(define dmenu
  (or (getenv "DMENU")
      "dmenu -i"))


;;; Shell
;; TODO find a way to return the error message in case the process fails.
(define (shell-eval->string command-str)
  "Execute COMMAND-STR and return the stdout, without the trailing
newline. On error or unexpected exit return #f."
  (let* ((port (open-pipe command-str OPEN_READ))
         (result (get-string-all port)))
    (if (or (not (= 0 (status:exit-val (close-pipe port))))
            (string-null? result))
        #f
        (string-drop-right result 1)))) ;remove trailing newline


;;; Dmenu helper functions
(define (dmenu-noyes prompt)
  "Return #t if the answer to the PROMPT is 'Yes' and #f otherwise."
  (let ((choice (shell-eval->string
                 (format #f "printf 'No\nYes' | ~a -p '~a'"
                         dmenu prompt))))
    (and (string? choice)
         (string= "Yes" choice))))

(define (dmenu-mkdir path)
  "If PATH does not exist, create it and return #t when successful and #f
otherwise. `dmenu-noyes' is used to confirm the creation of PATH."
  (or (file-exists? path)
      (and (dmenu-noyes (format #f "~a does not exist, create it?" path))
           (mkdir path))))              ;FIX: error when path is in /

;; TODO replace call to `find` with guile functions
(define (dmenu-choose-dir prompt depth . paths)
  "Use `find` with 'maxdepth' DEPTH to generate the completion
candidates for `dmenu'. Return the path chosen or #f otherwise."
  (let ((choice (shell-eval->string
                 (format #f "find ~a -maxdepth ~d -type d  2> /dev/null | ~a -p \"~a\" -l 10"
                         (string-join paths)
                         depth
                         dmenu
                         prompt))))
    (if (and choice (dmenu-mkdir choice))
        choice
        #f)))

(define (dmenu-choose-from string-list)
  "Return a list of strings chosen from STRING-LIST using `dmenu'. You
can choose multiple elements in dmenu using 'C-RET' (emacs notation)."
  (let* ((input (string-join string-list "\n"))
         (cmd (format #f "printf '~a' | ~a" input dmenu))
         (choice (shell-eval->string cmd)))
    (and choice
         (string-split choice #\newline))))


;;; Use notify-send to notify the user
(define* (notify-send title body #:key critical? notify-send)
  "Execute `notify-send` with TITLE and BODY. If CRITICAL? is not set add
'--urgency=critical' as a flag. Use NOTIFY-SEND to change the command
that will be executed."
  (let ((notify-send (or notify-send "notify-send")))
    (if critical?
        (system* notify-send "--urgency=critical" "-t" "5000"
                 (string-append "Error: " title)
                 body)
        (system* notify-send "-t" "5000"
                 (string-append "Done: " title)
                 body))))
