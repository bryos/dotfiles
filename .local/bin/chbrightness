#!/usr/bin/guile \
-e main -s
!#

;; Description:
;; Simple script to change the screen brightness. Requires root rights
;; or run:
;; `chgrp video /sys/class/backlight/brightness' and
;; `chmod g+w /sys/class/backlight/brightness' and
;; `usermod -aG video $USER'
;;
;; Dependencies:
;; - Guile Scheme

(use-modules (ice-9 textual-ports)) ;textual i/o
(use-modules (ice-9 match))

(define path "/sys/class/backlight/intel_backlight")
(define path-brightness (string-append path "/brightness"))
(define current-level
  (string->number
   (call-with-input-file path-brightness get-line)))
(define change 500)
(define max-level
  (or (string->number
       (call-with-input-file
           (string-append path "/max_brightness")
         get-line))
      20000))

(define (usage)
  (format (current-error-port)
          "\
Usage: chbrightness OPTION

OPTION:
  show, to show the current brightness
  +,    to increase the brightness by ~a
  -,    to decrease the brightness by ~a
  N,    where N is a number between 0 and ~a"
          change change max-level))

(define* (change-brightness func #:optional new-level)
  (let ((new-level (or new-level
                       (func current-level change))))
    (if (and (<= 0 new-level)
             (<= new-level max-level))
        (call-with-output-file path-brightness
          (lambda (port)
            (display new-level port)))
        (format (current-error-port)
                "The brightness must be 0 <= ~a <= ~a"
                new-level max-level))))

(define (main args)
  (define (first-arg arg . args) arg)
  (let* ((cli-arg (and (= 2 (length args))
                       (cadr args))))
    (match cli-arg
      ("-" (change-brightness -))
      ("+" (change-brightness +))
      ("show" (display current-level))
      (#f (usage))
      (else
       (let ((cli-num (string->number cli-arg)))
         (if cli-num
             (change-brightness first-arg cli-num)
             (usage)))))))
