#!/usr/bin/env python

# Description:
# Script to move the location of ~/.python_history
#
# sources:
# https://bugs.python.org/msg318437
# https://docs.python.org/3/library/readline.html?highlight=readline#example
#
# Dependencies:
# - python

import atexit
import os
import readline

_histdir = os.path.join(os.path.expanduser("~"), ".cache", "python")
_histfile = os.path.join(_histdir, "python_history")

os.makedirs(_histdir, mode=0o755, exist_ok=True)

try:
    readline.read_history_file(_histfile)
    # default history len is -1 (infinite), which may grow unruly
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, _histfile)
