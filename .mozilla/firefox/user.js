user_pref("browser.uidensity", 1);
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("extensions.activeThemeID", "firefox-compact-dark@mozilla.org");
user_pref("browser.download.dir", "~/documents/downloads");
// stop firefox from asking to add an application for mailto links
user_pref("network.protocol-handler.external.mailto", false);
