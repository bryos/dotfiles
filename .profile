#!/bin/sh
# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

### if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$XDG_CONFIG_HOME/bash/bashrc" ]; then
	    . "$XDG_CONFIG_HOME/bash/bashrc"
    elif [ -f "$HOME/.bashrc" ]; then
	    . "$HOME/.bashrc"
    fi
fi

### set PATH so it includes user's private bin if it exists
[ -d "$HOME/.local/mybin" ] && PATH="$PATH:$HOME/.local/mybin"
[ -d "$HOME/.local/bin"   ] && PATH="$PATH:$HOME/.local/bin"

##### Guix #####
# Make br-utils.scm available on nonguix systems (needed by some
# scripts)
export GUILE_LOAD_PATH=/home/br/my-projects/gitlab/dotfiles/.local/bin/.guile/modules

GUIX_PROFILE="$HOME/.guix-profile"
[ -f "$GUIX_PROFILE/etc/profile" ] && . "$GUIX_PROFILE/etc/profile"
GUIX_PROFILE="$HOME/.config/guix/current"
[ -f "$GUIX_PROFILE/etc/profile" ] && . "$GUIX_PROFILE/etc/profile"
GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
[ -f "$GUIX_LOCPATH" ] && export GUIX_LOCPATH="${HOME}/.guix-profile/lib/locale"

##### EXPORTS #####
# XDG User Directories
export XDG_DOCUMENTS_DIR="$HOME/documents"
export XDG_DESKTOP_DIR="$XDG_DOCUMENTS_DIR/desktop"
export XDG_DOWNLOAD_DIR="$XDG_DOCUMENTS_DIR/downloads"
export XDG_MUSIC_DIR="$XDG_DOCUMENTS_DIR/media/music"
export XDG_PICTURES_DIR="$XDG_DOCUMENTS_DIR/media/pictures"
export XDG_VIDEOS_DIR="$XDG_DOCUMENTS_DIR/media/videos"

# XDG Base Directories
export XDG_CACHE_HOME="$HOME/.cache"        # Analagous to /var/cache
export XDG_CONFIG_HOME="$HOME/.config"      #              /etc
export XDG_DATA_HOME="$HOME/.local/share"   #              /usr/share
export XDG_STATE_HOME="$HOME/.local/state"  #              /var/lib
export XDG_RUNTIME_DIR="/run/user/$(id -u)"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"
export XDG_CONFIG_DIRS="/etc/xdg"

### Scripting Variables
# Directories
export MY_BIN_DIR="$HOME/.local/mybin"  # My personal script directory
export MY_MAIN_USB="$XDG_DOCUMENTS_DIR/mnt/verbatim" # Mount point for my main usb
export MY_PROJECTS_DIR="$HOME/my-projects"
export DOTFILE_DIR="$MY_PROJECTS_DIR/gitlab/dotfiles"
export WALLPAPER_DIR="$MY_PROJECTS_DIR/gitlab/wallpapers/assets"
export ORG_DIR="$XDG_DOCUMENTS_DIR/org"
export SUCKLESS_DIR="$HOME/.local/suckless"
export BR_MAIL_DIR="${XDG_DOCUMENTS_DIR}/mail"
# Software
export BROWSER='firefox'
export DMENU='dmenu -i'
export EDITOR='emacsclient --create-frame --alternate-editor='
export SHELL='/bin/fish'
export SUDO='doas'  # replace with 'sudo -A' if using sudo instead of doas
export TERMINAL='st'
export THEME='dark'

### Software Settings
# Exa theming
export EXA_COLORS='da=38;5;67:di=38;5;148:ex=38;5;76:ln=38;5;72:lp=38;5;148'    # file types
export EXA_COLORS="$EXA_COLORS:ur=38;5;220:uw=38;5;160:ux=38;5;76:ue=38;5;76"   # permission bits
export EXA_COLORS="$EXA_COLORS:sn=38;5;76:sb=38;5;76"                           # file size
export EXA_COLORS="$EXA_COLORS:uu=38;5;220:un=38;5;214:gu=38;5;220:gn=38;5;214" # users and groups
export EXA_COLORS="$EXA_COLORS:*.mp3=38;5;105"                                  # file based on extension
export TERM='xterm-256color'            # getting proper colors

# Moving config files from $HOME to their proper XDG location
export CABAL_CONFIG="$XDG_CONFIG_HOME/cabal/config"  # Haskell
export CABAL_DIR="$XDG_CACHE_HOME/cabal"             # Haskell
export CARGO_HOME="$XDG_DATA_HOME/cargo"             # Rust
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export GHCUP_USE_XDG_DIRS='True'                     # Haskell
export GUILE_HISTORY="$XDG_CACHE_HOME/guile/guile_history"
# export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export HISTFILE="$XDG_STATE_HOME/bash/history"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME/jupyter"        # Jupyter (Python)
export LEDGER_FILE="${XDG_DOCUMENTS_DIR}/02-area/finance/current.journal"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export PYTHONSTARTUP="$MY_BIN_DIR/PYTHONSTARTUP"            # to move .python_history
export VIMINIT="source $XDG_CONFIG_HOME/nvim/init.vim"
#export VIMRUNTIME="source $XDG_CONFIG_HOME/vim/"
#export MYVIMRC="$XDG_CONFIG_HOME/vim/vimrc"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export SUDO_ASKPASS="/usr/local/bin/askpass-${DMENU%% *}"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export XAUTHORITY="$XDG_CONFIG_HOME/xorg/Xauthority"  # could break some DM's

# Software Settings
export MANPAGER='nvim +Man!'
export PASSWORD_STORE_DIR="${XDG_DOCUMENTS_DIR}/pass"

# Daemons
# if command -v guix >/dev/null && ! grep -q ID=guix /etc/os-release; then
#     ${SUDO} nscd > "${XDG_DATA_HOME}"/nscd.log 2>&1 # needed for guix
#     SHELL=/bin/bash emacs --daemon >/dev/null 2>&1 &
# fi

# Replace ssh-agent with gpg-agent
unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi

# Choose a graphical environment to start
. "${XDG_CONFIG_HOME:-$HOME/.config}"/bash/bash_profile
